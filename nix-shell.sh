#!/bin/bash
nix-shell -p mesa_glu -p glxinfo -p freetype -p xorg.libX11 -p xorg.glproto -p cmake -p xorg.libXinerama -p xorg.randrproto -p xorg.libXcursor -p xorg.libXrandr -p xorg.xrandr -p xorg.renderproto -p xorg.xf86vidmodeproto -p gdb -p sfml -p cmake --command ${SHELL}
