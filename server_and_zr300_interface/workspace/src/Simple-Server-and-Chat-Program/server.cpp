/*!
 * Simple chat program (server side).cpp - jchartou
 * Bassed off hassan yousef's server-code.
 */

#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

int main()
{
    int client, server;
    int portNum = 1500;
    bool isExit = false;
    constexpr int bufsize = 1024;
    size_t rgb_size = 480*360*3;
    uint8_t buffer[bufsize];
    uint8_t* img = new uint8_t[rgb_size];

    struct sockaddr_in server_addr;
    socklen_t size;

    /* ---------- ESTABLISHING SOCKET CONNECTION ----------*/
    /* --------------- socket() function ------------------*/

    client = socket(AF_INET, SOCK_STREAM, 0);

    if (client < 0) 
    {
        cout << "\nError establishing socket..." << endl;
        exit(1);
    }


    cout << "\n=> Socket server has been created..." << endl;


    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(portNum);

    /* ---------- BINDING THE SOCKET ---------- */
    /* ---------------- bind() ---------------- */


    if ((bind(client, (struct sockaddr*)&server_addr,sizeof(server_addr))) < 0) 
    {
        cout << "=> Error binding connection, the socket has already been established..." << endl;
        return -1;
    }


    size = sizeof(server_addr);

    /* ------------- LISTENING CALL ------------- */
    /* ---------------- listen() ---------------- */

    listen(client, 1);

    struct timeval tv = { 0 };
    tv.tv_sec = 2;
    tv.tv_usec = 0;

    int clientCount = 1;
    while(true) {
        cout << "=> Looking for clients..." << endl;

        server = accept(client,(struct sockaddr *)&server_addr,&size);

        setsockopt(server, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv,
                sizeof tv);
        setsockopt(server, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,
                sizeof tv);

        // first check if it is valid or not
        if (server < 0) {
            cout << "=> Error on accepting..." << endl;
            continue;
        }


        /* strcpy(buffer, "=> Server connected...\n"); */
        /* send(server, buffer, bufsize, 0); */
        cout << "=> Connected with the client #" << clientCount++ << ", you are good to go..." << endl;

        cout << "Client: \n";
        *buffer=' ';

        ssize_t err;
        while(true) {
            std::string s;
            std::getline(cin,s);
            *buffer = 'i';
            cout << "Sending request... ('" << *buffer << "')\n";
            if(0 >= (err = send(server,buffer,1,0))) {
                cout << "Send failed\n";
                break;
            }
            if(0 >= (err = recv(server, buffer, 1, 0))) {
                cout << "Recv failed\n";
                break;
            }
            if(0 >= (err = send(server, buffer, 1, 0))) {
                cout << "resend failed\n";
                break;
            }

            cout << "Got '" << *buffer << "'\n";

            if (*buffer == 's') {

                if(send(server,buffer,1,0) < 1) { break; }
                if(recv(server,buffer,4,0) < 4) { break; }
                size_t size = (size_t)buffer[0] |
                    (((size_t)buffer[1])<<8)  |
                    (((size_t)buffer[2])<<16) |
                    (((size_t)buffer[3])<<24);

                cout << "New size: " << size << "\n";

                if(rgb_size != size) {
                    rgb_size = size;
                    if(img) { delete [] img; }
                    img = new uint8_t[size];
                }
                size_t start = 0;
                while(start < rgb_size) {
                    err = recv(server,img+start,rgb_size-start,0);
                    if(err <= 0) { break; }
                    start += err;
                }
                if(start < rgb_size) {
                    cout << "Img recv failed: only got " << start
                         << " bytes\n";
                    break;
                }

                double avg = 0;
                for(size_t i = 0; i < err; ++i) {
                    avg += img[i];
                }
                avg /= err;

                cout << "Received img. " << err << " pixels. Avg pixel val: " << avg <<
                    std::endl;
            } else { break; }
        }

        cout << "done: " << err << "\n";
        close(server);
    }

    close(client);
    return 0;
}
