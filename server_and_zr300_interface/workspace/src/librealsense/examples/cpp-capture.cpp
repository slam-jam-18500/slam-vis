// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2015 Intel Corporation. All Rights Reserved.

#include <librealsense/rs.hpp>
#include "example.hpp"

#include <sstream>
#include <iostream>

#include <iomanip>
#include <thread>
#include <algorithm>
#include <map>
#include <memory>

//Added
#include <fstream>
#include <string>

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
//

//For ISP Connection (using some CSV+ includes)
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
using namespace std;
#include <inttypes.h>
//

texture_buffer buffers;//[RS_STREAM_COUNT];
bool align_depth_to_color = true;
bool align_color_to_depth = true;
bool color_rectification_enabled = true;

// Split the screen into 640X480 tiles, according to the number of supported streams. Define layout as follows : tiles -> <columds,rows>
const std::map<size_t, std::pair<int, int>> tiles_map = {       { 1,{ 1,1 } },
                                                                { 2,{ 2,1 } },
                                                                { 3,{ 2,2 } },
                                                                { 4,{ 2,2 } },
                                                                { 5,{ 3,2 } },          // E.g. five tiles, split into 3 columns by 2 rows mosaic
                                                                { 6,{ 3,2 } }};

int main(int argc, char * argv[]) try
{
    //Added: Add for possible print to CSV
    /*
    string filename;
    filename="testcsv.csv";
    ofstream fout;
    fout.open(filename);
    fout << "HELLO DONGS" <<","<<endl;
    fout.close();
    */
    //

    /* Make a client */
    /*1) Make information for TCP*/

    int client;
    int portNum = 1500;

    if(argc > 1) {
        portNum = atoi(argv[1]);
        if(portNum < 1000 || portNum >= 10000) {
            cout << "Invalid portNum: " << portNum << std::endl;
            return -1;
        }
    }

    bool isExit = false;
    constexpr int bufsize = 1024;
    uint8_t buffer[bufsize] = { 0 };
    struct sockaddr_in server_addr = { 0 };

    uint8_t rgb_buffsize=(uint8_t)(640*480);
    //char rgb_buffer[sizeof(uint8_t)*rgb_buffsize];
    string filename;
    filename="testcsv.csv";
        
    cout << "MADE IT HERE MY BEAUTIFUL BOI 1" <<endl;
    //2) Establishing Socket connection
    
    /*Socket function: Establish a function.
      AF_INET: Address: address domain of the socket.
      SOCK_STREAM: The type of socket, SOCK_STREAM=TCP.
      0: Protocol argument: Should always be 0.*/
    client = socket(AF_INET, SOCK_STREAM, 0);
    if (client < 0){//Check if client not established.
	cout << "\nCan't establish a socket! Ending now" << endl;
        exit(1);
    }
    cout << "Established a socket!" <<endl;
    
    /*Serv_Addr initilaization, Data structure of sin (socket info)
	sin_family should be AF_INET if address domain of socket is AF_INET.
	
        htons() converts port number in program to port number for networking.
    */
    server_addr.sin_family=AF_INET;
    server_addr.sin_port=htons(portNum);
    
    //3) Connecting the socket.
    int connect_status;
    cout << "Stuck in connect?" << endl;
    connect_status=connect(client,(struct sockaddr*)&server_addr,sizeof(server_addr));
    cout << "If this prints its not, hanging" << endl;
    if (connect_status==0){
	cout <<"Connected! To port number " << portNum << endl;
    }
    cout << "GOT HERE 2" << endl;
    

    //4) Recv, and messages.
    /*
	Recv recieves data from a connected socket, or bound connecitonless socket.
	Arg 1: Socket you want to use	
	Arg 2: Str Buffer you need.
	Arg 3: Length of the buffer.
	Arg 4: Flags in int, for this, just pick 0 for now.
    */
    cout << "Comfirming from server..." << endl;
    cout << "Connection confirmed, good to go!" << endl;
   
    //5) Close everything.
    rs::log_to_console(rs::log_severity::warn);
    rs::context ctx;
    if (ctx.get_device_count() == 0) throw std::runtime_error("No device detected. Is it plugged in?");
    rs::device & dev = *ctx.get_device(0);

    std::vector<rs::stream> supported_streams;
    float m_gyro_pos[3] = { M_PI/2,0,0 };
    float avg_gyro_vel[3] = { 0 };
    float m_gyro_vel[3] = { 0 };
    double last_timestamp = 0;
    bool first_motion = true;
    size_t frames = 0;

    for (int i = (int)rs::capabilities::depth; i <= (int)rs::capabilities::fish_eye; i++) {
        if (dev.supports((rs::capabilities)i))
            supported_streams.push_back((rs::stream)i);
        break;
    }

    if(dev.supports(rs::capabilities::motion_events)) {
        cout << "Enabling motion tracking\n";
        dev.enable_motion_tracking([&avg_gyro_vel, &m_gyro_vel,
                &first_motion, &last_timestamp, &m_gyro_pos,
                &frames](rs::motion_data e) {
            if (e.timestamp_data.source_id != RS_EVENT_IMU_GYRO) {
                return;
            }

            /* cout << "Motion track\n"; */

            if(!e.is_valid) {
                if(first_motion) {
                    first_motion = false;
                } else {

                    float dt = e.timestamp_data.timestamp
                               - last_timestamp;
                    m_gyro_pos[0] += (dt/1000)
                                     *(e.axes[0]-avg_gyro_vel[0]);
                    m_gyro_pos[1] += (dt/1000)
                                     *(e.axes[1]-avg_gyro_vel[1]);
                    m_gyro_pos[2] += (dt/1000)
                                     *(e.axes[2]-avg_gyro_vel[2]);
                    m_gyro_pos[0] = fmod(m_gyro_pos[0],2*M_PI);
                    m_gyro_pos[1] = fmod(m_gyro_pos[1],2*M_PI);
                    m_gyro_pos[2] = fmod(m_gyro_pos[2],2*M_PI);

                    /* cout << "(" << m_gyro_pos[0] << "," */
                    /*      << m_gyro_pos[1] << "," << m_gyro_pos[2] */
                    /*      << ")\n"; */
                    /* cout << "(" << m_gyro_vel[0] << "," */
                    /*      << m_gyro_vel[1] << "," << m_gyro_vel[2] */
                    /*      << ")\n"; */

                }
                last_timestamp = e.timestamp_data.timestamp;
                m_gyro_vel[0] = e.axes[0];
                m_gyro_vel[1] = e.axes[1];
                m_gyro_vel[2] = e.axes[2];

                if(frames < 200) {
                    avg_gyro_vel[0] =
                        (avg_gyro_vel[0]*frames+e.axes[0])/(frames+1);
                    avg_gyro_vel[1] =
                        (avg_gyro_vel[1]*frames+e.axes[1])/(frames+1);
                    avg_gyro_vel[2] =
                        (avg_gyro_vel[2]*frames+e.axes[2])/(frames+1);
                    ++frames;
                }
            }
        });
        dev.start(rs::source::motion_data);
    }
    if(dev.supports_option(rs::option::r200_lr_auto_exposure_enabled)) {
        std::cout << "Turning on auto exposure" << std::endl;
        dev.set_option(rs::option::r200_lr_auto_exposure_enabled, 1);
    }

    // Configure all supported streams to run at 30 frames per second
    for (auto & stream : supported_streams)
        dev.enable_stream(stream, rs::preset::best_quality);

    // Compute field of view for each enabled stream
    for (auto & stream : supported_streams)
    {
        if (!dev.is_stream_enabled(stream)) continue;
        auto intrin = dev.get_stream_intrinsics(stream);
        std::cout << "Capturing " << stream << " at " << intrin.width << " x " << intrin.height;
        std::cout << std::setprecision(1) << std::fixed << ", fov = " << intrin.hfov() << " x " << intrin.vfov() << ", distortion = " << intrin.model() << std::endl;
    }

    // Start our device
    dev.start();

    // Open a GLFW window
    glfwInit();
    std::ostringstream ss; ss << "CPP Capture Example (" << dev.get_name() << ")";

    int rows = 2;//tiles_map.at(supported_streams.size()).second;
    int cols = 2;//tiles_map.at(supported_streams.size()).first;
    int tile_w = 480; // pixels
    int tile_h = 360; // pixels
	//
    int show_screen=1;
    uint16_t* Zero_uint16_t=(uint16_t *)malloc(sizeof(uint16_t));
    int rgb_size=640*480*3;
    uint8_t* rgb_image = new uint8_t[rgb_size];
    std::vector<float> point_coords;
    int stride=0;
	//
    GLFWwindow * win = glfwCreateWindow(tile_w*cols, tile_h*rows, ss.str().c_str(), 0, 0);
    glfwSetWindowUserPointer(win, &dev);
    glfwSetKeyCallback(win, [](GLFWwindow * win, int key, int scancode, int action, int mods)
    {
        auto dev = reinterpret_cast<rs::device *>(glfwGetWindowUserPointer(win));
        if (action != GLFW_RELEASE) switch (key)
        {
        case GLFW_KEY_R: color_rectification_enabled = !color_rectification_enabled; break;
        case GLFW_KEY_C: align_color_to_depth = !align_color_to_depth; break;
        case GLFW_KEY_D: align_depth_to_color = !align_depth_to_color; break;
        case GLFW_KEY_E:
            if (dev->supports_option(rs::option::r200_emitter_enabled))
            {
                int value = !dev->get_option(rs::option::r200_emitter_enabled);
                std::cout << "Setting emitter to " << value << std::endl;
                dev->set_option(rs::option::r200_emitter_enabled, value);
            }
            break;
        case GLFW_KEY_A:
            if (dev->supports_option(rs::option::r200_lr_auto_exposure_enabled))
            {
                int value = !dev->get_option(rs::option::r200_lr_auto_exposure_enabled);
                std::cout << "Setting auto exposure to " << value << std::endl;
                dev->set_option(rs::option::r200_lr_auto_exposure_enabled, value);
            }
            break;
        }
    });
    glfwMakeContextCurrent(win);

    uint64_t depth_hist[256] = { 0 };

    while (!glfwWindowShouldClose(win))
    {
        // Wait for new images
        glfwPollEvents();
        dev.wait_for_frames();

        // Clear the framebuffer
        int w, h;
        glfwGetFramebufferSize(win, &w, &h);
        glViewport(0, 0, w, h);
        glClear(GL_COLOR_BUFFER_BIT);

        // Draw the images
        glPushMatrix();
        glfwGetWindowSize(win, &w, &h);
        glOrtho(0, 1.1*w, 1.1*h, 0, -1, +1);
	
	buffers.send_and_show(dev,rs::stream::depth,0,0,2*tile_w,2*tile_h,show_screen,Zero_uint16_t);

        glLineWidth(3);
        glBegin(GL_LINES);
            glColor3f(1,0,0);
            glVertex2f(0,0);
            glVertex2f(2*tile_w,0);
            glVertex2f(0,tile_h);
            glVertex2f(2*tile_w,tile_h);
            glVertex2f(0,2*tile_h);
            glVertex2f(2*tile_w,2*tile_h);

            glVertex2f(0,0);
            glVertex2f(0,2*tile_h);
            glVertex2f(tile_w,0);
            glVertex2f(tile_w,2*tile_h);
            glVertex2f(2*tile_w,0);
            glVertex2f(2*tile_w,2*tile_h);
        glEnd();
        glColor3f(1,1,1);

	//Possibly fetch and do all arguments here?
	const void* data=dev.get_frame_data(rs::stream::depth);
        auto points = reinterpret_cast<const rs::float3 *>
            (dev.get_frame_data(rs::stream::points));
	const uint16_t *depth_image=reinterpret_cast<const uint16_t *>(data);
	int width=dev.get_stream_width(rs::stream::depth);
	int height=dev.get_stream_height(rs::stream::depth);	

        if(rgb_size != width*height*3) {
            rgb_size = width*height*3;
            if(rgb_image) { delete [] rgb_image; }
            rgb_image = new uint8_t[width*height*3];
        }
	//Time to write to teh csv!	
        buffers.make_depth_histogram_method(rgb_image,depth_image,width,height,filename);
        for(size_t i = 0; i < width*height*3; ++i) {
            ++depth_hist[rgb_image[i]];
        }

        if(1 <= recv(client,buffer,1,MSG_DONTWAIT)) {
            std::cout << "Received '" << *buffer << "'\n";

            cout << "(" << m_gyro_pos[0] << ","
                << m_gyro_pos[1] << "," << m_gyro_pos[2]
                << ")\n";

            if (*buffer=='i'){
                *buffer='s';
                send(client,buffer,1,0);
                recv(client,buffer,1,0);
                //Get new message...
                double avg = 0;
                double avg_z = 0;
                for(size_t i = 0; i < rgb_size; ++i) {
                    avg += rgb_image[i];
                    avg_z += points[i/3].z;
                }
                avg /= rgb_size;
                avg_z /= rgb_size;
                cout << "RGB Size: " << rgb_size << "\n";
                buffer[0] = (width       )&0xff;
                buffer[1] = (width>>(1*8))&0xff;
                buffer[2] = (width>>(2*8))&0xff;
                buffer[3] = (width>>(3*8))&0xff;
                buffer[4] = (height       )&0xff;
                buffer[5] = (height>>(1*8))&0xff;
                buffer[6] = (height>>(2*8))&0xff;
                buffer[7] = (height>>(3*8))&0xff;

                size_t w = (size_t)buffer[0] |
                    (((size_t)buffer[1])<<8) |
                    (((size_t)buffer[2])<<16) |
                    (((size_t)buffer[3])<<24);
                size_t h = (size_t)buffer[4] |
                    (((size_t)buffer[5])<<8) |
                    (((size_t)buffer[6])<<16) |
                    (((size_t)buffer[7])<<24);
                cout << "Size: " << w << " by " << h << "\n";
                point_coords.clear();
                point_coords.reserve(rgb_size);
                for(size_t i=0; i < rgb_size; i+=3) {
                    auto p = points[i/3];
                    if(p.z) {
                        point_coords.push_back(p.x);
                        point_coords.push_back(p.y);
                        point_coords.push_back(p.z);
                    }
                }

                size_t start;

                if(8 > (send(client,buffer,8,0))) {
                    cout << "Size send failed\n";
                } else if(sizeof(m_gyro_pos) > send(client,
                            &m_gyro_pos[0], sizeof(m_gyro_pos), 0)) {
                    cout << "Gyro send failed\n";
                } else {
                    cout << "Sent gyro pos: (" << m_gyro_pos[0] << ","
                        << m_gyro_pos[1] << "," << m_gyro_pos[2]
                        << ")\n";
                    ssize_t err;
                    start = 0;
                    while(start < rgb_size) {
                        err = send(client,rgb_image+start,rgb_size-start,0);
                        if(err < 1) {
                            break;
                        }
                        start += err;
                    }
                    if(start < rgb_size) {
                        cout << "Send Failed: only got " << start
                            << " bytes (" << strerror(errno) << ")\n";
                    } else {
                        size_t num_floats = point_coords.size();
                        std::cout << "Sending point array size: " <<
                            num_floats << "\n";
                        if((err = send(client, &num_floats,
                                        sizeof(num_floats), 0)) <= 0) {
                            cout << "Point array size send failed: "
                                << strerror(errno) << "\n";
                        } else {
                            start = 0;
                            while(start < num_floats*sizeof(float)) {
                                err = send(client,
                                        ((uint8_t*)&point_coords[0])+start,
                                        num_floats*sizeof(float)-start, 0);
                                if(err < 1) {
                                    break;
                                }
                                start += err;
                            }
                            if(start < num_floats*sizeof(float)) {
                                cout << "Point array send failed with "
                                     << start << " bytes sent: "
                                     << strerror(errno) << "\n";
                            }
                        }
                    }


                    cout << "Sent. Avg pixel val: " << avg << std::endl;
                    cout << "      Avg point z val: " << avg_z << std::endl;
                }

            }
        }

	//send(client,(char*)rgb_image[1000],rgb_buffsize,0);
	//cout << "value random is " <<unsigned(rgb_image[1000])<<endl;
        glPopMatrix();
        glfwSwapBuffers(win);
    }
    //Not working
    free(Zero_uint16_t);
    //fout.close();
    cout << "\nClosing Client!\n";
    close(client);
    //Or is it
    glfwDestroyWindow(win);
    glfwTerminate();

    cout << "\n\n\n";

    double depth_hist_total = 0;
    for(size_t i = 0; i < 256; ++i) {
        depth_hist_total += depth_hist[i];
    }
    for(size_t i = 0; i < 256; ++i) {
        printf("  %03lu: %lf\n",i,depth_hist[i]/depth_hist_total);
    }

    return EXIT_SUCCESS;
}
catch (const rs::error & e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception & e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
