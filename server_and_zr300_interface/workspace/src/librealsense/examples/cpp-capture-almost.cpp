// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2015 Intel Corporation. All Rights Reserved.

#include <librealsense/rs.hpp>
#include "example.hpp"

#include <sstream>
#include <iostream>

#include <iomanip>
#include <thread>
#include <algorithm>
#include <map>
#include <memory>

//Added
#include <fstream>
#include <string>

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
//

//For ISP Connection (using some CSV+ includes)
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
using namespace std;
#include <inttypes.h>
//

texture_buffer buffers;//[RS_STREAM_COUNT];
bool align_depth_to_color = true;
bool align_color_to_depth = true;
bool color_rectification_enabled = true;

// Split the screen into 640X480 tiles, according to the number of supported streams. Define layout as follows : tiles -> <columds,rows>
const std::map<size_t, std::pair<int, int>> tiles_map = {       { 1,{ 1,1 } },
                                                                { 2,{ 2,1 } },
                                                                { 3,{ 2,2 } },
                                                                { 4,{ 2,2 } },
                                                                { 5,{ 3,2 } },          // E.g. five tiles, split into 3 columns by 2 rows mosaic
                                                                { 6,{ 3,2 } }};

int main(int argc, char * argv[]) try
{
    //Added: Add for possible print to CSV
    /*
    string filename;
    filename="testcsv.csv";
    ofstream fout;
    fout.open(filename);
    fout << "HELLO DONGS" <<","<<endl;
    fout.close();
    */
    //

    /* Make a client */
    /*1) Make information for TCP*/
    
    int client;
    int portNum = 1500;
    bool isExit = false;
    int bufsize = 1024;
    char buffer[bufsize];
    char* ip = "127.0.0.1";
    struct sockaddr_in server_addr;

    uint8_t rgb_buffsize=(uint8_t)(640*480*3);
    //char rgb_buffer[sizeof(uint8_t)*rgb_buffsize];
    int test[1];
    test[0]=3;
    string filename;
    filename="testcsv.csv";
    ofstream fout;
    fout.open(filename);
	    
    //char read_csv='4';
    cout << "MADE IT HERE MY BEAUTIFUL BOI 1" <<endl;
    //2) Establishing Socket connection
    
    /*Socket function: Establish a function.
      AF_INET: Address: address domain of the socket.
      SOCK_STREAM: The type of socket, SOCK_STREAM=TCP.
      0: Protocol argument: Should always be 0.*/
    client = socket(AF_INET, SOCK_STREAM, 0);
    if (client < 0){//Check if client not established.
	cout << "\nCan't establish a socket! Ending now" << endl;
        exit(1);
    }
    cout << "Established a socket!" <<endl;
    
    /*Serv_Addr initilaization, Data structure of sin (socket info)
	sin_family should be AF_INET if address domain of socket is AF_INET.
	
        htons() converts port number in program to port number for networking.
    */
    server_addr.sin_family=AF_INET;
    server_addr.sin_port=htons(portNum);
    
    //3) Connecting the socket.
    int connect_status;
    cout << "Stuck in connect?" << endl;
    connect_status=connect(client,(struct sockaddr*)&server_addr,sizeof(server_addr));
    cout << "If this prints its not, hanging" << endl;
    if (connect_status==0){
	cout <<"Connected! To port number " << portNum << endl;
    }
    cout << "GOT HERE 2" << endl;
    

    //4) Recv, and messages.
    /*
	Recv recieves data from a connected socket, or bound connecitonless socket.
	Arg 1: Socket you want to use	
	Arg 2: Str Buffer you need.
	Arg 3: Length of the buffer.
	Arg 4: Flags in int, for this, just pick 0 for now.
    */
    cout << "Comfirming from server..." << endl;
    //*buffer='!';
    //send(client,buffer,bufsize,0);
    //recv(client,buffer,bufsize,0);
    cout << "Connection confirmed, good to go!" << endl;
    //5) Send message!
    /*
	Send: Sends data on buffer to the connected port number.
    */
    /*
    while(!isExit){
	cout << "Client : ";
	//Send, idk, like a 0 or something.
	
	*buffer=(char)'0';
	send(client,buffer,bufsize,0);
	//Added to close server.
    	*buffer=(char)'#';
    	send(client,buffer,bufsize,0);
	//
	isExit=true;
    }
    */
    //5) Close everything.
    //cout << "\nClosing Client!\n";
    //close(client);
    /* Done with make a client */
    //
    rs::log_to_console(rs::log_severity::warn);
        rs::context ctx;
    if (ctx.get_device_count() == 0) throw std::runtime_error("No device detected. Is it plugged in?");
    rs::device & dev = *ctx.get_device(0);

    std::vector<rs::stream> supported_streams;

    for (int i = (int)rs::capabilities::depth; i <= (int)rs::capabilities::fish_eye; i++)
        if (dev.supports((rs::capabilities)i))
            supported_streams.push_back((rs::stream)i);

    // Configure all supported streams to run at 30 frames per second
    for (auto & stream : supported_streams)
        dev.enable_stream(stream, rs::preset::best_quality);

    // Compute field of view for each enabled stream
    for (auto & stream : supported_streams)
    {
        if (!dev.is_stream_enabled(stream)) continue;
        auto intrin = dev.get_stream_intrinsics(stream);
        std::cout << "Capturing " << stream << " at " << intrin.width << " x " << intrin.height;
        std::cout << std::setprecision(1) << std::fixed << ", fov = " << intrin.hfov() << " x " << intrin.vfov() << ", distortion = " << intrin.model() << std::endl;
    }

    // Start our device
    dev.start();

    // Open a GLFW window
    glfwInit();
    std::ostringstream ss; ss << "CPP Capture Example (" << dev.get_name() << ")";

    int rows = tiles_map.at(supported_streams.size()).second;
    int cols = tiles_map.at(supported_streams.size()).first;
    int tile_w = 640; // pixels
    int tile_h = 480; // pixels
	//
    int show_screen=1;
    uint16_t* Zero_uint16_t=(uint16_t *)malloc(sizeof(uint16_t));
    int rgb_size=tile_w*tile_h*3;
    uint8_t rgb_image[rgb_size];
    int stride=0;
	//
    GLFWwindow * win = glfwCreateWindow(tile_w*cols, tile_h*rows, ss.str().c_str(), 0, 0);
    glfwSetWindowUserPointer(win, &dev);
    glfwSetKeyCallback(win, [](GLFWwindow * win, int key, int scancode, int action, int mods)
    {
        auto dev = reinterpret_cast<rs::device *>(glfwGetWindowUserPointer(win));
        if (action != GLFW_RELEASE) switch (key)
        {
        case GLFW_KEY_R: color_rectification_enabled = !color_rectification_enabled; break;
        case GLFW_KEY_C: align_color_to_depth = !align_color_to_depth; break;
        case GLFW_KEY_D: align_depth_to_color = !align_depth_to_color; break;
        case GLFW_KEY_E:
            if (dev->supports_option(rs::option::r200_emitter_enabled))
            {
                int value = !dev->get_option(rs::option::r200_emitter_enabled);
                std::cout << "Setting emitter to " << value << std::endl;
                dev->set_option(rs::option::r200_emitter_enabled, value);
            }
            break;
        case GLFW_KEY_A:
            if (dev->supports_option(rs::option::r200_lr_auto_exposure_enabled))
            {
                int value = !dev->get_option(rs::option::r200_lr_auto_exposure_enabled);
                std::cout << "Setting auto exposure to " << value << std::endl;
                dev->set_option(rs::option::r200_lr_auto_exposure_enabled, value);
            }
            break;
        }
    });
    glfwMakeContextCurrent(win);
    
    while (!glfwWindowShouldClose(win))
    {
        // Wait for new images
        glfwPollEvents();
        dev.wait_for_frames();

        // Clear the framebuffer
        int w, h;
        glfwGetFramebufferSize(win, &w, &h);
        glViewport(0, 0, w, h);
        glClear(GL_COLOR_BUFFER_BIT);

        // Draw the images
        glPushMatrix();
        glfwGetWindowSize(win, &w, &h);
        glOrtho(0, w, h, 0, -1, +1);
        //buffers.show(dev,rs::stream::depth,0,0,2*tile_w,2*tile_h);
	
	buffers.send_and_show(dev,rs::stream::depth,0,0,2*tile_w,2*tile_h,show_screen,Zero_uint16_t);
	//printf("%" PRIu16 "\n",*Zero_uint16_t);
	//Possibly fetch and do all arguments here?
	const void* data=dev.get_frame_data(rs::stream::depth);
	const uint16_t *depth_image=reinterpret_cast<const uint16_t *>(data);
	int width=dev.get_stream_width(rs::stream::depth);
	int height=dev.get_stream_height(rs::stream::depth);	
	//recv(client,buffer,bufsize,0);

	//Time to write to teh csv!	
	if (*buffer==(char)'4'){
		buffers.make_depth_histogram(rgb_image,depth_image,width,height);
		//Write info from rgb_image to csv!
		/*for (int j=0;j<1;j++){//rgb_size
	    		fout << std::to_string(rgb_image[j])<<",";
	    	}*/

		*buffer=(char)'2';
		send(client,buffer,bufsize,0);
	}
	//CSV is done writing! Now let server program use information.
	else{
		*buffer=(char)'3';
		send(client,buffer,bufsize,0);
	}
	//Get new message...
	recv(client,buffer,bufsize,0);



	//send(client,(char*)rgb_image[1000],rgb_buffsize,0);
	//cout << "value random is " <<unsigned(rgb_image[1000])<<endl;
        glPopMatrix();
        glfwSwapBuffers(win);
    }
    //Not working
    free(Zero_uint16_t);
    fout.close();
    cout << "\nClosing Client!\n";
    close(client);
    //Or is it
    glfwDestroyWindow(win);
    glfwTerminate();
    return EXIT_SUCCESS;
}
catch (const rs::error & e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception & e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
