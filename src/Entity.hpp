#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Vector.hpp"
#include "AABB.hpp"
#include <vector>

class Entity {
public:
    static const float MAX_RESOLUTION_SPEED = 10;

    static void applyKinematics(Entity& e,float dt);
    static void handleCollision(Collision c,Entity& e);

private:
    unsigned _collisionPriority;

protected:
    Vec3f _loc,_vel;

    virtual void _update(float dt)=0;

    Entity(unsigned collisionPriority = 2) :
        _collisionPriority(collisionPriority) {}

public:
    std::vector<Vec3f> forces;

    virtual AABB collisionBox() const=0;

    void update(float dt);

    unsigned collisionPriority() const {
        return _collisionPriority;
    }
    Vec3f position() const {
        return _loc;
    }
    Vec3f velocity() const {
        return _vel;
    }
    double heading() const {
        return _heading;
    }
};

#endif
