#ifndef SERVER_HPP
#define SERVER_HPP

#include <vector>
#include "thread_queue.hpp"
#include "Vector.hpp"
#include "Mat44.hpp"

struct realsense_depth_img {
    float angles[3];
    size_t w,h;
    std::vector<uint8_t> pixels;
    std::vector<Vec3f> points;
};

void img_grab_server(int portNum,thread_queue<bool>& poke,
        thread_queue<realsense_depth_img>& out);
void vive_loc_server(int portNum,thread_queue<bool>& poke,
        thread_queue<std::pair<uint8_t,Mat44f>>& out);

#endif

