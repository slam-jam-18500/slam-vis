#ifndef AABB_HPP
#define AABB_HPP

#include "Vector.hpp"
#include "Maybe.hpp"
#include <cassert>

struct Collision {
    Vec3f overlap;

    Collision() = default;
    Collision(const Collision& other) = default;
    Collision(Vec3f _overlap) : overlap(_overlap) {}
};

struct AABB {
    Vec3f center,size;

    AABB() = default;
    AABB(const AABB& other) = default;
    AABB(Vec3f _center,Vec3f _size) : center(_center),size(_size) {}

    bool operator==(const AABB& other) const {
        return center == other.center && size == other.size;
    }
    bool operator!=(const AABB& v) const {
        return !(*this == v);
    }

    bool contains(const Vec3f& p) const {
        /* assert(size[0] >= 0 && size[1] >= 0 && size[2] >= 0); */
        /* Vec3f maxbounds = center+size/2; */
        /* Vec3f maxp = p + size/2; */

        /* return maxp[0] >= center[0] && p[0] <= maxbounds[0] && */
        /*        maxp[1] >= center[1] && p[1] <= maxbounds[1] && */
        /*        maxp[2] >= center[2] && p[2] <= maxbounds[2]; */

        Vec3f maxbounds = center+0.5f*size;
        Vec3f minbounds = center-0.5f*size;

        assert(maxbounds[0] >= minbounds[0]);
        assert(maxbounds[1] >= minbounds[1]);
        assert(maxbounds[2] >= minbounds[2]);

        return minbounds[0] <= p[0] && p[0] <= maxbounds[0] &&
               minbounds[1] <= p[1] && p[1] <= maxbounds[1] &&
               minbounds[2] <= p[2] && p[2] <= maxbounds[2];
    }

    bool contains(const AABB& other) const {
        for(int x = -1; x <= 1; x += 2) {
            for(int y = -1; y <= 1; y += 2) {
                for(int z = -1; z <= 1; z += 2) {
                    Vec3f p = other.center +
                        Vec3f{{x*other.size[0]/2,
                               y*other.size[1]/2,
                               z*other.size[2]/2}};
                    if(!contains(p)) { return false; }
                }
            }
        }
        return true;

    }

    Maybe<Collision> checkCollision(const AABB& other,
                                    bool calcMinOverlap=true) const;
};



#endif
