#define GL_GLEXT_PROTOTYPES
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <GL/glext.h>
#include <GL/glu.h>
#include "Color.hpp"
#include "Camera.hpp"
#include "NumUtil.hpp"
#include "OctTree.hpp"
#include "Triangle.hpp"
#include "Ray.hpp"
#include "thread_pool.hpp"
#include "thread_queue.hpp"
#include "server.hpp"
#include "rw_mutex.hpp"

#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>

#include <signal.h>

/* #include <sys/types.h> */
/* #include <sys/sockets.h> */


const float FPS = 60;
const float DT  = 1.0/FPS;
const sf::Time TIME_PER_FRAME = sf::seconds(DT);
/*const float WORLD_RADIUS = 200;*/
const float CAM_SPEED = 2;
const float LOOK_SCALE = 1.0/10;
const float CLICKS_PER_SECOND = 4;
const float MAX_RESOLUTION_SPEED = 10;
const std::string TITLE = "SlamVis OctTree Test";

void initViewport(int width,int height,Camera& camera) {
    glViewport(0,0,width,height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    camera.aspectRatio = (height != 0) ? width/(float)height : 1;
}

void initGL() {
    glClearColor(0x37/255.0,0x87/255.0,0xcd/255.0,0);
    glClearDepth(1);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    /* glEnable(GL_COLOR_MATERIAL); */
    /* glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE); */
    /* glEnable(GL_LIGHTING); */
    /* glEnable(GL_LIGHT1); */

    glDisable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
}

/* void send_data_thread(thread_queue& q, */

float frand() {
    return rand()/(float)RAND_MAX;
}

int main(int argc, char** argv) {
    signal(SIGPIPE,SIG_IGN);

    int realsense_port = 1500;
    int vive_port = 2099;

    if(argc > 1) {
        realsense_port = atoi(argv[1]);
        if(realsense_port < 1000 || realsense_port >= 10000) {
            std::cout << "Bad realsense port: " << realsense_port
                      << std::endl;
            return -1;
        }
    }

    if(argc > 2) {
        vive_port = atoi(argv[2]);
        if(vive_port < 1000 || vive_port >= 10000) {
            std::cout << "Bad vive port: " << vive_port
                      << std::endl;
            return -1;
        }
    }

    std::cout << "DT: " << DT << std::endl;
    sf::Window window(sf::VideoMode(640,480),
                      TITLE,
                      sf::Style::Default,
                      sf::ContextSettings(32));
    {
        auto settings = window.getSettings();
        std::cout << "Depth bits:    " << settings.depthBits         << "\n"
                  << "Stencil bits:  " << settings.stencilBits       << "\n"
                  << "AA Level:      " << settings.antialiasingLevel << "\n"
                  << "Major version: " << settings.majorVersion      << "\n"
                  << "Minor version: " << settings.minorVersion      << "\n";
    }
    bool mouseCaptured = false;
    sf::Vector2u windowSize = window.getSize();
    sf::Vector2i windowCenter(windowSize.x/2,
                              windowSize.y/2);
    int windowWidth  = windowSize.x,
        windowHeight = windowSize.y;
    assert(windowWidth != 0 && windowHeight != 0);
    Camera camera;
    /* Maybe<Vec3f> scanpos,scandir; */
    GLUquadric* quadric = gluNewQuadric();
    gluQuadricDrawStyle(quadric,GLU_FILL);

    constexpr float rs_angle = -57;

    thread_queue<bool> img_grab_poke;
    thread_queue<realsense_depth_img> img_grab_out;
    thread_queue<bool> vive_loc_poke;
    thread_queue<std::pair<uint8_t,Mat44f>> vive_loc_out;
    Vec3f space_origin{{0.0f,-2.609f,-3.27f-1.155f+0.7f}};
    Vec3f user_loc{{100.0f,100.0f,100.0f}};
    Vec3f user_dir{{1.0f,0.0f,0.0f}};

    std::thread server_thread([realsense_port,&img_grab_poke,&img_grab_out]() {
            img_grab_server(realsense_port,img_grab_poke,img_grab_out);
    });
    std::thread vive_loc_thread([vive_port,&vive_loc_poke,&vive_loc_out]() {
            vive_loc_server(vive_port,vive_loc_poke,vive_loc_out);
    });

    initGL();
    initViewport(windowWidth,windowHeight,camera);

    camera.loc = Vec3f{{2.6,1,-2.6}};
    camera.heading = -133;
    camera.pitch = -16.9;

    OctTree<std::unordered_set<Triangle3D>> tree;
    OctTree<std::unordered_set<Ray>> ray_tree;
    std::unique_ptr<std::unordered_set<Ray>> to_insert(new
            std::unordered_set<Ray>);
    std::unique_ptr<std::unordered_set<Ray>> to_delete(new
            std::unordered_set<Ray>);
    std::mutex ray_tree_lock;
    std::mutex to_insert_lock;
    rw_mutex to_delete_lock;
    std::vector<Vec3f> frame_verts;

    std::vector<Vec3f> scanpoints;
    std::vector<Vec3f> scandirs;


    tree.box.center = {{0,0,0}};
    tree.box.size = /*{{1,1,1}};*/{{1.5,1.5,1.5}};
    /* { */
    /*     Vec3f minbounds = {{0,0,0}}, maxbounds = {{0,0,0}}; */

    /*     tinyobj::attrib_t attrib; */
    /*     std::vector<tinyobj::shape_t> shapes; */
    /*     tinyobj::LoadObj(&attrib, &shapes, NULL, NULL, */
    /*             "data/spot_triangulated.obj",NULL,true); */

    /*     for(auto shape: shapes) { */
    /*         const auto& mesh = shape.mesh; */
    /*         for(size_t i = 0; i < mesh.indices.size(); i += 3) { */
    /*             assert(mesh.num_face_vertices[i/3] == 3); */
    /*             Triangle3D tri; */
    /*             int ix[] = { */
    /*                 mesh.indices[i  ].vertex_index, */
    /*                 mesh.indices[i+1].vertex_index, */
    /*                 mesh.indices[i+2].vertex_index */
    /*             }; */
    /*             for(size_t a=0; a < 3; ++a) { */
    /*                 for(size_t b=0; b < 3; ++b) { */
    /*                     tri.verts[a][b] = */
    /*                         attrib.vertices[ix[a]*3 + b]; */

    /*                     minbounds[b] = */
    /*                         std::min(minbounds[b],tri.verts[a][b]); */
    /*                     maxbounds[b] = */
    /*                         std::max(maxbounds[b],tri.verts[a][b]); */

    /*                 } */
    /*             } */

    /*             tree.value.insert(tri); */
    /*             frame_verts.push_back(tri.verts[0]); */
    /*             frame_verts.push_back(tri.verts[1]); */
    /*             frame_verts.push_back(tri.verts[2]); */
    /*         } */

    /*         std::cout << tree.value.size() << " triangles\n"; */
    /*         std::cout << frame_verts.size() << " vertices\n"; */
    /*     } */

    /*     tree.box.size   = 1.2f*(maxbounds-minbounds); */
    /*     tree.box.center = (minbounds/2 + maxbounds/2); */

    /* } */

    std::condition_variable update_notify;
    std::condition_variable updated_notify;
    std::mutex update_notify_lock;
    bool done = false;

    Color3f base_color = GREEN;
    std::vector<Color3f> colors[2];
    std::vector<Vec3f> verts[2];
    rw_mutex resize_lock[2];
    std::atomic<size_t> draw_ind[2];
    draw_ind[0].store(0); draw_ind[1].store(0);
    std::atomic<size_t> insert_ind[2];
    insert_ind[0].store(0); insert_ind[1].store(0);

    std::atomic<size_t> dirty[2];
    dirty[0].store(1); dirty[1].store(1);

    bool update_carved = false;

    std::function<void(const AABB&)> take_box_verts =
            [&colors,&verts,&base_color,&insert_ind,&draw_ind,&resize_lock,&update_carved](const AABB& box) {
        size_t which = update_carved;
        /* std::cout << "Box: " << box.center << ", " << box.size << std::endl; */
        for(size_t i = 0; i < 3; ++i) {
            for(int s = -1; s <= 1; s += 2) {
                size_t j = (i+1)%3;
                size_t k = (j+1)%3;

#ifndef NDEBUG
                /* std::cout << "(" << i << "," << j << "," */
                /*                  << k << "), " << s << "\n"; */
#endif

                Vec3f face_center = box.center;
                face_center[i] += s*box.size[i]/2;

                Vec3f corners[4] = {
                    face_center,face_center,face_center,face_center
                };

                corners[0][j] += box.size[j]/2;
                corners[0][k] += box.size[k]/2;

                corners[1][j] -= box.size[j]/2;
                corners[1][k] += box.size[k]/2;

                corners[2][j] -= box.size[j]/2;
                corners[2][k] -= box.size[k]/2;

                corners[3][j] += box.size[j]/2;
                corners[3][k] -= box.size[k]/2;

                Triangle3D tri1 =
                    {{ corners[0], corners[1], corners[2] }};
                auto tri1_normal = cross(tri1.verts[1]-tri1.verts[0],
                        tri1.verts[2]-tri1.verts[0]);
                if(dot(face_center-box.center,tri1_normal) < 0) {
                    std::swap(tri1.verts[0],tri1.verts[2]);
                    tri1_normal = cross(tri1.verts[1]-tri1.verts[0],
                        tri1.verts[2]-tri1.verts[0]);
                    assert(dot(face_center-box.center,tri1_normal) >=
                            0);
                }
                Triangle3D tri2 =
                    {{ corners[2], corners[3], corners[0] }};
                auto tri2_normal = cross(tri2.verts[1]-tri2.verts[0],
                        tri2.verts[2]-tri2.verts[0]);
                if(dot(face_center-box.center,tri2_normal) < 0) {
                    std::swap(tri2.verts[0],tri2.verts[2]);
                    tri2_normal = cross(tri2.verts[1]-tri2.verts[0],
                        tri2.verts[2]-tri2.verts[0]);
                    assert(dot(face_center-box.center,tri2_normal) >=
                            0);
                }

                resize_lock[which].r_lock();
                while(insert_ind[which].load()+6 >= verts[which].size()) {
                    resize_lock[which].r_unlock();
                    {
                        std::lock_guard<rw_mutex::mutex_wrapper>
                            l(resize_lock[which].w_mutex());
                        //while(draw_ind.load() != insert_ind.load()) {}
                        size_t new_size = insert_ind[which].load()+7;
                        verts[which].resize(2*new_size);
                        colors[which].resize(2*new_size);
                    }
                    resize_lock[which].r_lock();
                }
                size_t insert_pt = insert_ind[which].fetch_add(6);

                resize_lock[which].r_unlock();

                size_t verts_insert  = insert_pt;
                size_t colors_insert = insert_pt;

                {
                    std::lock_guard<rw_mutex::mutex_wrapper>
                        l(resize_lock[which].r_mutex());

                    verts[which][verts_insert++] = tri1.verts[0];

                    Color3f color = /*wrong ? RED :*/ (base_color * (1.0f +
                                0.2*(frand()-2)));
                    colors[which][colors_insert++] = color;
                    /* std::cout << "{" << tri1.verts[0] << ","; */
                    verts[which][verts_insert++] = tri1.verts[1];
                    colors[which][colors_insert++] = color;
                    /* std::cout << tri1.verts[1] << ","; */
                    verts[which][verts_insert++] = tri1.verts[2];
                    colors[which][colors_insert++] = color;
                    /* std::cout << tri1.verts[2] << "}, "; */

                    verts[which][verts_insert++] = tri2.verts[0];
                    colors[which][colors_insert++] = color;
                    /* std::cout << "{" << tri2.verts[0] << ","; */
                    verts[which][verts_insert++] = tri2.verts[1];
                    colors[which][colors_insert++] = color;
                    /* std::cout << tri2.verts[1] << ","; */
                    verts[which][verts_insert++] = tri2.verts[2];
                    colors[which][colors_insert++] = color;
                    /* std::cout << tri2.verts[2] << "}\n"; */

                    while(!draw_ind[which].compare_exchange_weak(insert_pt,insert_pt+6))
                        {}
                }
            }
        }
    };
    std::function<void(const
            OctTree<std::unordered_set<Triangle3D>>&)>
        take_verts = [&colors,&verts,&take_box_verts](const
            OctTree<std::unordered_set<Triangle3D>>& tree) {
        AABB box = tree.box;
#ifndef NDEBUG
        for(const auto& v: tree.value) {
            assert(v.intersects(box));
        }
#endif
        /* assert(!tree.value.empty()); */

        /* bool has_children = false; */
        /* for(size_t x = 0; x < 2; ++x) { */
        /*     for(size_t y = 0; y < 2; ++y) { */
        /*         for(size_t z = 0; z < 2; ++z) { */
        /*             if(tree.subtrees[x][y][z]) { */
        /*                 has_children = true; */
        /*             } */
        /*         } */
        /*     } */
        /* } */

        /* bool wrong = (has_children != !tree.value.empty()); */
        /* if(wrong) { */
        /*     for(const auto& v: tree.value) { */
        /*         verts.push_back(1.001f*v.verts[0]); */
        /*         verts.push_back(1.001f*v.verts[1]); */
        /*         verts.push_back(1.001f*v.verts[2]); */
        /*         colors.push_back(RED); */
        /*         colors.push_back(RED); */
        /*         colors.push_back(RED); */
        /*     } */
        /* } */

        take_box_verts(box);
    };

    ray_tree.box = tree.box;
    ray_tree.box.size *= 1.5;

    bool draw_carved = true;
    bool minimize = false;
    size_t tesselation_depth[2] = { 6,6 };
    auto noop_lam = [](const OctTree<std::unordered_set<Ray>>&) {};
    std::function<void (const
            OctTree<std::unordered_set<Ray>>&)> noop =
        noop_lam;
    auto noop_emp_lam = [](const AABB&) {};
    std::function<void (const
            AABB&)> noop_emp = noop_emp_lam;

    thread_pool pool(6);
    thread_pool ray_pool(1);
    std::function<void(std::function<void()>)> run_fn =
            [&pool](std::function<void()> f) {
        pool.add_job(f);
    };
    std::function<void()> barrier = [&pool]() {
        pool.barrier();
    };

    auto update_verts_core = [&minimize, &draw_carved, &update_carved,
         &ray_tree, &tree, &colors, &verts, &tesselation_depth,
         &take_verts, &take_box_verts, &barrier, &noop, &noop_emp, &base_color,
         &resize_lock, &ray_tree_lock, &insert_ind, &draw_ind, &pool,
         &run_fn, &dirty, &to_insert, &to_insert_lock, &to_delete,
         &to_delete_lock, &ray_pool]() {

        std::lock_guard<std::mutex> l(ray_tree_lock);

        size_t which = draw_carved;

        if(!dirty[which].load()) { return; }

        update_carved = draw_carved;

        if(draw_carved) {
            /* ray_pool.barrier(); */
            {
                std::lock_guard<std::mutex> l(to_insert_lock);
                if(to_insert->size()) {
                    ray_tree.tesselated = false;
                    std::cout << "Inserting...\n";
                    for(auto x: *to_insert) {
                        ray_tree.value.insert(x);
                        to_delete->insert(x);
                    }
                    std::function<void(const
                            OctTree<std::unordered_set<Ray>>&)> save_needed
                            = [&to_delete,&to_delete_lock](const
                                    OctTree<std::unordered_set<Ray>>&
                                    leaf) {
                        bool has_redundancy = false;
                        {
                            std::lock_guard<rw_mutex::mutex_wrapper>
                                read_l(to_delete_lock.r_mutex());
                            for(auto x: leaf.value) {
                                if(!to_delete->count(x)) {
                                    has_redundancy = true;
                                    break;
                                }
                            }
                        }
                        if(!has_redundancy) {
                            /* std::cout << "Necessity!\n"; */
                            std::lock_guard<rw_mutex::mutex_wrapper>
                                read_l(to_delete_lock.w_mutex());
                            for(auto x: leaf.value) {
                                to_delete->erase(x);
                            }
                        }

                    };

                    std::cout << "Checking redundancies...\n";
                    for_each_leaf<Ray>(ray_tree,
                            tesselation_depth[which], save_needed,
                            noop_emp, run_fn);
                    pool.barrier();

                    std::cout << "Should I trim?\n";
                    if(to_delete->size() > 1000) {
                        std::cout << "Trimming...\n";
                        ray_tree.tesselated = false;
                        for(auto x: *to_delete) {
                            ray_tree.value.erase(x);
                        }
                        for(size_t x = 0; x < 2; ++x) {
                            for(size_t y = 0; y < 2; ++y) {
                                for(size_t z = 0; z < 2; ++z) {
                                    ray_tree.subtrees[x][y][z].reset();
                                }
                            }
                        }
                        to_delete.reset(new std::unordered_set<Ray>);
                    }
                    to_insert.reset(new std::unordered_set<Ray>);
                    /* to_insert.clear(); */
                }
            }
            if(minimize) {
                minimize_tree(ray_tree,tesselation_depth[which],run_fn,barrier);
            }
            pool.barrier();

            base_color = BLUE;
            {
                std::lock_guard<rw_mutex::mutex_wrapper>
                    l(resize_lock[which].w_mutex());
                verts[which].clear();
                colors[which].clear();
                insert_ind[which].store(0);
                draw_ind[which].store(0);
            }
            for_each_leaf<Ray>(ray_tree, tesselation_depth[which], noop,
                    take_box_verts,run_fn);
        } else {
            {
                std::lock_guard<rw_mutex::mutex_wrapper>
                    l(resize_lock[which].w_mutex());
                verts[which].clear();
                colors[which].clear();
                insert_ind[which].store(0);
                draw_ind[which].store(0);
            }
            base_color = GREEN;
            for_each_leaf<Triangle3D>(tree,
                    tesselation_depth[(int)draw_carved], take_verts,
                    {}, run_fn);
        }
        pool.barrier();
        --dirty[which];

        std::cout << "How many elements? "
                  << (draw_carved ? ray_tree.value.size()
                                  : tree.value.size())
                  << std::endl;
        std::cout << "How many vertices? " << verts[which].size()
                  << std::endl;
        std::cout << "AABB center: " << tree.box.center << std::endl;
        std::cout << "AABB size: "   << tree.box.size << std::endl;
        std::cout << "Carved AABB center: " << ray_tree.box.center << std::endl;
        std::cout << "Carved AABB size: "   << ray_tree.box.size << std::endl;
    };

    size_t update_sig_ind = 0;
    std::atomic<bool> updater_working;
    updater_working.store(false);
    std::thread bg_updater([&update_verts_core, &update_notify,
            &update_notify_lock,&done,&update_sig_ind,&updater_working]() {
        size_t last_sig = update_sig_ind;
        while(!done) {
            last_sig = update_sig_ind;
            updater_working.store(true);
            update_verts_core();
            updater_working.store(false);
            {
                std::unique_lock<std::mutex> l(update_notify_lock);
                while(!done && last_sig == update_sig_ind) {
                    update_notify.wait(l);
                }
            }
        }
    });

    auto update_verts = [&update_notify,&update_sig_ind,&update_notify_lock]() {
        update_notify_lock.lock();
        ++update_sig_ind;
        update_notify_lock.unlock();
        update_notify.notify_all();
    };

    update_verts();

    auto increase_depth =
        [&draw_carved,&tesselation_depth,update_verts,&dirty]() {
        if(tesselation_depth[(int)draw_carved] < 100) {
            ++tesselation_depth[(int)draw_carved];
            ++dirty[(int)draw_carved];
            update_verts();
        }
    };

    auto decrease_depth =
        [&draw_carved,&tesselation_depth,update_verts,&dirty]() {
        if(tesselation_depth[(int)draw_carved] > 0) {
            --tesselation_depth[(int)draw_carved];
            ++dirty[(int)draw_carved];
            update_verts();
        }
    };

    bool just_draw_depth = false;

    // Code template taken from
    // https://www.opengl.org/discussion_boards/showthread.php/176958-Drawing-depth-texture-bound-to-a-FBO
    GLuint fbo;
    GLuint depth_target;
    GLuint depth_texture;
    {
        // Allocate objects
        glGenFramebuffers(1,&fbo);
        glGenTextures(1,&depth_target);
        glGenTextures(1,&depth_texture);

        // initialize depth texture
        glBindTexture(GL_TEXTURE_2D,depth_target);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, windowWidth,
                windowHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT,//GL_UNSIGNED_BYTE,
                NULL);
        /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, */
        /*         GL_LINEAR); */
        /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, */
        /*         GL_LINEAR); */
        /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, */
        /*         GL_CLAMP_TO_EDGE); */
        /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, */
        /*         GL_CLAMP_TO_EDGE); */
        glBindTexture(GL_TEXTURE_2D,0);

        // initialize Framebuffer object
        glBindFramebuffer(GL_FRAMEBUFFER,fbo);
        glDrawBuffer(GL_NONE); // no color texture
        glReadBuffer(GL_NONE);
        // render depth to depth_target
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                GL_TEXTURE_2D, depth_target, 0);

        // Verify that it initialized correctly
        {
            GLenum err = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if(GL_FRAMEBUFFER_COMPLETE != err) {
                std::cerr << "Could not initialize depth framebuffer: "
                        << err << std::endl;
                std::cerr << " (" << gluErrorString(err) << ")"
                        << std::endl;
                std::cout << std::endl;
                std::cout << std::endl;
                std::cout << std::endl;
                sf::milliseconds(10000);
                /* std::cerr << "Could not initialize depth framebuffer: " */
                /*         << err << " (" << gluErrorString(err) << ")" */
                /*         << std::endl; */
                exit(0);

            }
        }

        // reset to default framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER,0);
    }
    std::vector<float> pixels;
    std::vector<float> newpixels;

    sf::Clock frameTime;

    while(window.isOpen()) {
        frameTime.restart();

        sf::Event e;
        while(window.pollEvent(e)) {
            switch(e.type) {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::Resized:
                windowSize = window.getSize();
                windowWidth  = windowSize.x,
                windowHeight = windowSize.y;
                windowCenter.x = windowWidth/2;
                windowCenter.y = windowHeight/2;
                initViewport(windowWidth,windowHeight,camera);
                glBindTexture(GL_TEXTURE_2D,depth_target);
                glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, windowWidth,
                        windowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
                        NULL);
                glBindTexture(GL_TEXTURE_2D,0);
                break;
            case sf::Event::KeyPressed:
                if(e.key.code == sf::Keyboard::Escape) {
                    mouseCaptured = !mouseCaptured;
                    window.setMouseCursorVisible(!mouseCaptured);
                    if(mouseCaptured) {
                        sf::Mouse::setPosition(windowCenter,window);
                    }
                    std::cout << " Loc:     " << camera.loc << std::endl;
                    std::cout << " Heading: " << camera.heading << std::endl;
                    std::cout << " Pitch:   " << camera.pitch << std::endl;
                } else if(e.key.code == sf::Keyboard::Up) {
                    increase_depth();
                } else if(e.key.code == sf::Keyboard::Down) {
                    decrease_depth();
                } else if(e.key.code == sf::Keyboard::O) {
                    just_draw_depth = !just_draw_depth;
                    std::cout << " Depth rendering " <<
                        (just_draw_depth ? "on" : "off") << std::endl;
                } else if(e.key.code == sf::Keyboard::Return) {
                    std::stringstream ss;
                    ss << "" << time(NULL) << ".txt";
                    std::cout << "Writing out to " << ss.str() <<
                        std::endl;
                    {
                        std::fstream of(ss.str().c_str(),
                                std::ios::out);
                        for(size_t i = 0; i < draw_ind[1].load(); ++i)
                            {
                                of << verts[1][i][0] << ","
                                   << verts[1][i][1] << ","
                                   << verts[1][i][2] << " ";
                            }
                        of << "\n";
                        of.flush();
                        of.close();
                    }
                    std::cout << " Done writing!\n";
                } else if(e.key.code == sf::Keyboard::M) {
                    minimize = !minimize;
                    std::cout << " Carved-model minimization " <<
                        (minimize ? "on" : "off") << std::endl;
                    if(minimize) { ++dirty[1]; }
                    update_verts();
                } else if(e.key.code == sf::Keyboard::I) {
                    /* draw_carved = !draw_carved; */
                    /* std::cout << " Carved-model rendering " << */
                    /*     (draw_carved ? "on" : "off") << std::endl; */
                    /* update_verts(); */
                } else if(e.key.code == sf::Keyboard::Period) {
                } else if(e.key.code == sf::Keyboard::Comma) {
                    std::cout << "Clearing carved-tree\n";
                    if(!updater_working.load()) {
                        ray_tree.clear();
                        scanpoints.clear();
                        scandirs.clear();
                        ++dirty[1];
                        update_verts();
                    }
                }
                break;
            case sf::Event::LostFocus:
                mouseCaptured = false;
                window.setMouseCursorVisible(!mouseCaptured);
                break;
            case sf::Event::MouseButtonPressed:
                if(e.mouseButton.button == sf::Mouse::Left) {
                    /* img_grab_poke.push(true); */

                    /* auto ray_img = img_grab_out.pop(1000); */
                    /* while(ray_img) { */
                    /*     ray_img.forward([&ray_tree,&camera,&dirty,&update_verts,&draw_carved] */
                    /*                         (realsense_depth_img img) { */
                    /*         std::cout << "Capturing depth-map rays\n"; */
                    /*         std::cout << " Image w: " << img.w << ", h: " */
                    /*                 << img.h << "\n"; */
                    /*         std::cout << " Image angles: (" */
                    /*                   << img.angles[0] << "," */
                    /*                   << img.angles[1] << "," */
                    /*                   << img.angles[2] << ")\n"; */

                    /*         // 32 inches -> 56 */
                    /*         // 27 inches -> 46 */
                    /*         // 58 inches -> 99 */
                    /*         // y = 5.907944515e-1 x - 5.832282472e-1 */
                    /*         // (y = dist, x = depth) */
                    /*         float nearPlane = -0.583228; */
                    /*         float farPlane = 0.590794*255 + */
                    /*             nearPlane; */

                    /*         ray_tree.tesselated = false; */
                    /*         Vec2f center = */
                    /*             0.5f*Vec2f{{(float)img.w,(float)img.h}}; */
                    /*         /1* Vec3f out_dir = camera.viewDirection(); *1/ */
                    /*         Vec4f out_dir4 = */
                    /*             Mat44f::rotation(img.angles[0]*180/M_PI,1,0,0) */
                    /*             *Mat44f::rotation(img.angles[1]*180/M_PI,0,1,0) */
                    /*             *Vec4f{{0,0,1,1}};//camera.loc; */
                    /*         Vec3f out_dir{{out_dir4[0], out_dir4[1], */
                    /*                     out_dir4[2]}}; */

                    /*         float avg_dist = 0; */
                    /*         for(size_t i=0; i<img.pixels.size(); ++i) { */
                    /*             float dist = nearPlane + */
                    /*                 (farPlane-nearPlane)/255.0f*img.pixels[i]; */
                    /*             avg_dist += dist; */
                    /*         } */
                    /*         avg_dist /= img.pixels.size(); */
                    /*         out_dir *= 1.2*avg_dist/12.0; */

                    /*         Vec3f origin = -out_dir; */
                    /*         out_dir = norm(out_dir); */

                    /*         std::cout << " Rays from " << origin */
                    /*                   << " -> " << out_dir << std::endl; */
                    /*         Vec3f right = */
                    /*             norm(cross(out_dir,Vec3f{{0,1,0}})); */
                    /*         Vec3f up = */
                    /*             norm(cross(right,out_dir)); */

                    /*         float fovy = 54; */
                    /*         float aspectRatio = img.w/(float)img.h; */
                    /*         float radFovy = fovy*M_PI/180; */
                    /*         float halfVert = std::tan(radFovy/2); */
                    /*         float halfHoriz = halfVert*aspectRatio; */

                    /*         for(size_t y = 0; y < img.h; ++y) { */
                    /*             for(size_t x = 0; x < img.w; ++x) { */
                    /*                 Vec2f offset = */
                    /*                     Vec2f{{(float)x+0.5f,(float)(y+0.5f)}}-center; */
                    /*                 offset[1] *= -1; */
                    /*                 offset[0] /= (img.w/2.0f); */
                    /*                 offset[1] /= (img.h/2.0f); */

                    /*                 /1* std::cout << " Ray at (" << x << "," *1/ */
                    /*                 /1*     << y << "): (" << offset[0] << "," *1/ */
                    /*                 /1*     << offset[1] << ")\n"; *1/ */

                    /*                 Vec3f dir = out_dir + */
                    /*                     offset[0]*halfHoriz*right + */
                    /*                     offset[1]*halfVert*up; */
                    /*                 /1* std::cout << " " << dir << "\n"; *1/ */
                    /*                 /1* std::cout << " (" << dot(dir,out_dir) *1/ */
                    /*                 /1*     << ")\n"; *1/ */
                    /*                 float dist = */
                    /*                     img.pixels[3*(y*img.w+x)]/255.0; */
                    /*                 /1* std::cout << "  dist: " << dist << *1/ */
                    /*                 /1*     "\n"; *1/ */
                    /*                 if(dist < 5.0/255 || dist > 240.0/255) { */
                    /*                     continue; */
                    /*                 } */

                    /*                 dist *= */
                    /*                     farPlane-nearPlane; */
                    /*                 dist += nearPlane; */
                    /*                 dist *= 1.0/12; // inches -> feet */
                    /*     /1*             /2* std::cout << "dist = " << dist << *2/ *1/ */
                    /*     /1*             /2*     std::endl; *2/ *1/ */

                    /*                 // fudge-factor: underestimation is better */
                    /*                 dist *= 0.8; */


                    /*                 Ray r = Ray{origin,dist*dir}; */
                    /*                 if(r.intersects(ray_tree.box)) { */
                    /*                     /1* std::cout << "Intersects!\n"; *1/ */
                    /*                     ray_tree.value.insert(r); */
                    /*                 } */
                    /*             } */
                    /*         } */
                    /*         std::cout << ray_tree.value.size() */
                    /*                 << " rays\n"; */
                    /*         ++dirty[1]; */
                    /*         if(draw_carved) { */
                    /*             update_verts(); */
                    /*         } */
                    /*     }); */

                    /*     ray_img = img_grab_out.try_pop(); */
                    /* } */

                    /* /1* if(just_draw_depth && !draw_carved && *1/ */
                    /* /1*         (int)newpixels.size() == *1/ */
                    /* /1*         windowWidth*windowHeight*3) { *1/ */
                    /* /1*     std::cout << "Capturing depth-map rays\n"; *1/ */

                    /* /1*     ray_tree.tesselated = false; *1/ */

                    /* /1*     Vec2f center = *1/ */
                    /* /1*         0.5f*Vec2f{{(float)windowWidth,(float)windowHeight}}; *1/ */
                    /* /1*     Vec3f out_dir = camera.viewDirection(); *1/ */
                    /* /1*     Vec3f origin = camera.loc; *1/ */
                    /* /1*     Vec3f right = *1/ */
                    /* /1*         norm(cross(out_dir,Vec3f{{0,1,0}})); *1/ */
                    /* /1*     Vec3f up = *1/ */
                    /* /1*         norm(cross(right,out_dir)); *1/ */

                    /* /1*     float radFovy = camera.fovy*M_PI/180; *1/ */
                    /* /1*     float halfVert = std::tan(radFovy/2); *1/ */
                    /* /1*     float halfHoriz = halfVert*camera.aspectRatio; *1/ */

                    /* /1*     for(int y = 0; y < windowHeight; y+=3) { *1/ */
                    /* /1*         for(int x = 0; x < windowWidth; x+=3) { *1/ */
                    /* /1*             Vec2f offset = *1/ */
                    /* /1*                 Vec2f{{(float)x+0.5f,(float)(y+0.5f)}}-center; *1/ */
                    /* /1*             offset[0] /= (windowWidth/2.0f); *1/ */
                    /* /1*             offset[1] /= (windowHeight/2.0f); *1/ */

                    /* /1*             /2* std::cout << " Ray at (" << x << "," *2/ *1/ */
                    /* /1*             /2*     << y << "): (" << offset[0] << "," *2/ *1/ */
                    /* /1*             /2*     << offset[1] << ")\n"; *2/ *1/ */

                    /* /1*             Vec3f dir = out_dir + *1/ */
                    /* /1*                 offset[0]*halfHoriz*right + *1/ */
                    /* /1*                 offset[1]*halfVert*up; *1/ */
                    /* /1*             /2* std::cout << " " << dir << "\n"; *2/ *1/ */
                    /* /1*             /2* std::cout << " (" << dot(dir,out_dir) *2/ *1/ */
                    /* /1*             /2*     << ")\n"; *2/ *1/ */
                    /* /1*             float dist = *1/ */
                    /* /1*                 newpixels[3*(y*windowWidth+x)]; *1/ */
                    /* /1*             dist *= *1/ */
                    /* /1*                 camera.farPlane-camera.nearPlane; *1/ */
                    /* /1*             dist += camera.nearPlane; *1/ */
                    /* /1*             /2* std::cout << "dist = " << dist << *2/ *1/ */
                    /* /1*             /2*     std::endl; *2/ *1/ */

                    /* /1*             Ray r = Ray{origin,dist*dir}; *1/ */
                    /* /1*             if(r.intersects(ray_tree.box)) { *1/ */
                    /* /1*                 /2* std::cout << "Intersects!\n"; *2/ *1/ */
                    /* /1*                 ray_tree.value.insert(r); *1/ */
                    /* /1*             } *1/ */
                    /* /1*         } *1/ */
                    /* /1*     } *1/ */
                    /* /1*     std::cout << ray_tree.value.size() *1/ */
                    /* /1*               << " rays\n"; *1/ */
                    /* /1*     ++dirty[1]; *1/ */
                    /* /1* } *1/ */

                }
                break;
            default:
                break;
            }
        }

        auto vive_loc = vive_loc_out.try_pop();
        vive_loc.forward([&](std::pair<uint8_t,Mat44f> p) {
            bool use_img = (p.first == 1);
            if(p.first == 2) {
                Vec4f new_origin = (p.second * Vec3f{{0,0,0}})
                               * 3.28084f; // meters -> feet
                space_origin = Vec3f{{new_origin[0], new_origin[1],
                                      new_origin[2]}};
                std::cout << "\n\n===== UPDATING ORIGIN =====\n  "
                          << space_origin << "\n\n";
            }
            std::function<Mat44f (const Mat44f&)> wrap =
                    [&space_origin](const Mat44f& m) {
                return Mat44f::translation(-space_origin)
                    * Mat44f::scale(3.28084f, 3.28084f, 3.28084f)
                    * m
                    * Mat44f::rotation(rs_angle, 1, 0, 0)
                    * Mat44f::scale(1/3.28084f, 1/3.28084f,1/3.28084f)
                    * Mat44f::translation(0, 0.16, -0.18);
            };
            if(!use_img) {
                /* std::cout << "Ignoring non-capture event\n"; */
                auto user_loc4 = wrap(p.second) * Vec3f{{0,0,0}};
                auto user_dir4 = (wrap(p.second) * Vec3f{{0,0,-1}})
                    - user_loc4;
                user_loc = Vec3f{{user_loc4[0], user_loc4[1],
                                  user_loc4[2]}};
                user_dir = norm(Vec3f{{user_dir4[0], user_dir4[1],
                                  user_dir4[2]}});
                return;
            }
            auto m = p.second;

            m = wrap(m);

            for(size_t i=0; i < 4; ++i) {
                printf("   %+ 4.3f %+ 4.3f %+ 4.3f %+ 4.3f\n",
                        m.values[4*0+i],m.values[4*1+i],
                        m.values[4*2+i],m.values[4*3+i]);
            }

            while(img_grab_out.try_pop());

            img_grab_poke.push(true);

            auto ray_img = img_grab_out.pop(1000);
            /* img_grab_poke.push(true); */
            /* auto ray_img2 = img_grab_out.pop(1000); */
            while(ray_img) {// && ray_img2) {
                ray_img.forward(/*[&](realsense_depth_img img) {
                ray_img2.forward(*/[space_origin,/*img,*/m, use_img, &scanpoints, &scandirs,
                        &ray_tree, &camera, &dirty, &update_verts,
                        wrap, &draw_carved, &ray_pool, &ray_tree_lock,
                        &to_insert, &to_insert_lock, &vive_loc_out,
                        rs_angle]
                        (realsense_depth_img img) {
                    auto m2 = m;

                    vive_loc_out.pop().forward([wrap,&m2,rs_angle](std::pair<uint8_t,Mat44f> p) {
                        m2 = wrap(p.second);
                    });


                    for(size_t i=0; i < 4; ++i) {
                        printf("   %+ 4.3f %+ 4.3f %+ 4.3f %+ 4.3f\n",
                                m2.values[4*0+i],m2.values[4*1+i],
                                m2.values[4*2+i],m2.values[4*3+i]);
                    }

                    ray_pool.add_job([space_origin,m2, use_img, &scanpoints, &scandirs,
                            &ray_tree, &camera, &dirty, &update_verts,
                            &draw_carved,img,/*img2,*/&ray_tree_lock,
                            &to_insert, &to_insert_lock]() {
                        realsense_depth_img my_img = img;
                        auto my_m = m2;

                        std::cout << "Capturing depth-map rays\n";
                        std::cout << " Image w: " << img.w << ", h: "
                        << img.h << "\n";
                        /* std::cout << " Image2 w: " << img2.w << ", h: " */
                        /* << img2.h << "\n"; */
                        /* if(img.w != img2.w || img.h != img2.h */
                        /*    || img.points.size() != img2.points.size()) { */
                        /*     return; */
                        /* } */
                        std::cout << " Image angles: ("
                        << img.angles[0] << ","
                        << img.angles[1] << ","
                        << img.angles[2] << ")\n";

                        std::cout << " Waiting to own insert set "
                                << "finish...\n";

                        std::lock_guard<std::mutex> l(to_insert_lock);

                        std::cout << " Done!\n";


                        // 32 inches -> 56
                        // 27 inches -> 46
                        // 58 inches -> 99
                        // y = 5.907944515e-1 x - 5.832282472e-1
                        // (y = dist, x = depth)
                        float nearPlane = -0.583228;
                        float farPlane = 0.590794*255 +
                        nearPlane;

                        Vec2f center =
                            0.5f*Vec2f{{(float)img.w,(float)img.h}};
                        /* Vec3f out_dir = camera.viewDirection(); */
                        /* Vec4f origin4 = m * Vec3f{{0,0,0}}; */
                        Vec4f origin4 = my_m * Vec3f{{0,0,0}};
                        Vec4f out_dir4 =(my_m *
                                Vec3f{{0,0,-1}})-origin4;
                        Vec4f right4 = (my_m * Vec3f{{1,0,0}})-origin4;
                        Vec4f up4 = (my_m * Vec3f{{0,1,0}})-origin4;

                        Vec3f out_dir{{out_dir4[0], out_dir4[1],
                            out_dir4[2]}};
                        out_dir = norm(out_dir);
                        Vec3f right{{right4[0], right4[1],
                            right4[2]}};
                        right = norm(right);
                        Vec3f up{{up4[0], up4[1],
                            up4[2]}};
                        up = norm(up);

                        Vec3f origin{{origin4[0], origin4[1],
                            origin4[2]}};

                        /* Vec4f out_dir4 = */
                        /*     Mat44f::rotation(img.angles[0]*180/M_PI,-1,0,0) */
                        /*     *Mat44f::rotation(img.angles[1]*180/M_PI,0,-1,0) */
                        /*     *Vec4f{{0,0,-1,1}};//camera.loc; */

                        /* Vec4f origin4 = m * Vec4f{{0,0,0,1}}; */
                        /* Vec3f out_dir{{out_dir4[0], out_dir4[1], */
                        /*             out_dir4[2]}}; */
                        /* Vec3f origin{{origin4[0], origin4[1], */
                        /*     origin4[2]}}; */
                        /* origin *= 3.28084; // meters -> feet */
                        /* origin -= space_origin; */
                        /* my_m[12] = origin[0]; */
                        /* my_m[13] = origin[1]; */
                        /* my_m[14] = origin[2]; */
                        /* origin[0] -=    0; */
                        /* origin[1] -= -1.0+(-1.609); */
                        /* origin[2] -= -3.27-1.155+0.7; */

                        scanpoints.push_back(origin);
                        scandirs.push_back(out_dir);

                        /* scanpos = origin; */
                        /* scandir = out_dir; */

                        std::cout << " Rays from " << origin
                            << " -> " << out_dir << std::endl;
                        std::cout << "   Angle = "
                                  << (180/M_PI*
                                      std::atan(out_dir[1]
                                        /(1e-8+ std::sqrt(
                                            out_dir[0]*out_dir[0] +
                                            out_dir[2]*out_dir[2]))))
                                    << "deg\n";
                        std::cout << " Up = " << up
                            << ", Right = " << right << std::endl;


                        float fovy = 45;
                        float aspectRatio = img.w/(float)img.h;
                        float radFovy = fovy*M_PI/180;
                        float halfVert = std::tan(radFovy/2);
                        float halfHoriz = halfVert*aspectRatio;

                        double avg_dist = 0;
                        double middle_avg_dist = 0;
                        double max_dist = 0;
                        float max_x = -100, min_x = 100,
                              max_y = -100, min_y = 100;
                        size_t n = 0;
                        size_t middle_n = 0;

                        for(size_t i=0; i < img.points.size(); ++i) {
                            auto v1 = img.points[i];
                            /* auto v2 = img2.points[i]; */
                            if(!v1[2]) {// || !v2[2]) {
                                continue;
                            }
                            v1 *= 3;//.28084;

                            max_x = std::max(max_x,v1[0]);///v1[2]);
                            min_x = std::min(min_x,v1[0]);///v1[2]);
                            max_y = std::max(max_y,v1[1]);///v1[2]);
                            min_y = std::min(min_y,v1[1]);///v1[2]);

                            /* if(std::abs(std::max(v2[2]/v1[2],v1[2]/v2[2])) > */
                            /*         1.1) { */
                            /*     std::cout << " Too noisy!\n"; */
                            /*     continue; */
                            /* } */

                            /* v1 += 0.5f*(v2-v1); */
                            auto p1 = Vec3f{{0,0,0}};
                            auto p2 = p1 +
                                Vec3f{{v1[0],-v1[1],-v1[2]}};
                            auto dir4 = (my_m * p2 - my_m*p1);
                            v1[0] = dir4[0];
                            v1[1] = dir4[1];
                            v1[2] = dir4[2];
                            auto dir = my_img.points[i] = v1;
                                /* float dist = */
                                /*     img.pixels[3*(y*img.w+x)]/255.0; */
                                /* float dist2 = */
                                /*     img2.pixels[3*(y*img2.w+x)]/255.0; */
                        /* } */
                        /* for(size_t y = img.h/4; y < 0.75*img.h; ++y) { */
                        /*     for(size_t x = img.w/4; x < (0.75*img.w); ++x) { */
                        /*         Vec2f offset = */
                        /*             Vec2f{{(float)x+0.5f,(float)(y+0.5f)}}-center; */
                        /*         offset[1] *= -1; */
                        /*         offset[0] /= (img.w/2.0f); */
                        /*         offset[1] /= (img.h/2.0f); */

                                /* std::cout << " Ray at (" << x << "," */
                                /*     << y << "): (" << offset[0] << "," */
                                /*     << offset[1] << ")\n"; */

                                /* Vec3f dir = out_dir + */
                                /*     offset[0]*halfHoriz*right + */
                                /*     offset[1]*halfVert*up; */
                                /* std::cout << " " << dir << "\n"; */
                                /* std::cout << " (" << dot(dir,out_dir) */
                                /*     << ")\n"; */
                                /* std::cout << "  dist: " << dist << */
                                /*     "\n"; */

                            ///////////////////
                                Ray r =
                                    Ray{origin-(1.0f/12)*norm(right),
                                    2*(1.0f/12)*norm(right)};
                                if(r.intersects(ray_tree.box)) {
                                    /* std::cout << "Intersects!\n"; */
                                    to_insert->insert(r);
                                }
                                r = Ray{origin-(1.0f/12)*norm(up),
                                    2*(1.0f/12)*norm(up)};
                                if(r.intersects(ray_tree.box)) {
                                    /* std::cout << "Intersects!\n"; */
                                    to_insert->insert(r);
                                }
                                r = Ray{origin-(1.0f/12)*norm(out_dir),
                                    2*(1.0f/12)*norm(out_dir)};
                                if(r.intersects(ray_tree.box)) {
                                    /* std::cout << "Intersects!\n"; */
                                    to_insert->insert(r);
                                }
                            ///////////////////

                                if(!use_img) {// || dir.mag() < (2.0)/12
                                            /* || dir.mag() > 5) { */
                                    continue;
                                }

                                /* dist = 0.5*(dist + dist2);// - 2.0/255; */

                                /* dist *= */
                                /*     farPlane-nearPlane; */
                                /* dist += nearPlane; */
                                /* dist *= 1.0/12; // inches -> feet */
                                /* /1*             /2* std::cout << "dist = " << dist << *2/ *1/ */
                                /*             /1*     std::endl; *1/ */

                                // fudge-factor: underestimation is better
                                /* dist *= 0.7; */
                                /* dist *= 0.9; */
                                /* dist = std::max(0.0f,dist-6.0f/12); */


                                ++n;
                                avg_dist += dir.mag();
                                if(std::abs(img.points[i][1]/img.points[i][2]) <
                                        std::tan(15*M_PI/180) &&
                                   std::abs(img.points[i][0]/img.points[i][2]) <
                                        std::tan(15*M_PI/180)) {
                                    middle_avg_dist += dir.mag();
                                    ++middle_n;
                                }
                                avg_dist += dir.mag();
                                max_dist = std::max((double)dir.mag(),max_dist);

                            }
                        /* } */
                        avg_dist /= (n+1e-6);
                        middle_avg_dist /= (middle_n + 1e-6);
                        for(size_t i=0; i < my_img.points.size(); ++i) {
                            auto v1 = my_img.points[i];
                            /* auto v2 = img2.points[i]; */
                            if(!use_img || !v1[2]) {// || !v2[2]) {
                                continue;
                            }

                            auto dir = v1;

                            if(std::max(dir.mag()/(middle_avg_dist+1e-6),
                                        middle_avg_dist/dir.mag())
                                    > 1.3) {
                                continue;
                            }

                            auto base4 = my_m *
                                Vec3f{{0,0,0}};
                            auto base = Vec3f{{base4[0], base4[1],
                                base4[2]}};

                            auto r = Ray{base,dir};
                            if(r.intersects(ray_tree.box)) {
                                /* std::cout << "Intersects!\n"; */
                                to_insert->insert(r);
                            }
                        }
                        double avg_pt_dist = 0;
                        for(auto p: img.points) {
                            avg_pt_dist += p[2];
                        }
                        std::cout << "Max dist: "
                                  << max_dist << std::endl;
                        std::cout << "Raw bounds - x: ("
                                  << min_x << "," << max_x << ") y: ("
                                  << min_y << "," << max_y << ")\n";
                        std::cout << "Avg dist: "
                                  << avg_dist << std::endl;
                        std::cout << "Middle-Avg dist: "
                                  << middle_avg_dist << std::endl;
                        std::cout << "Avg pt dist: "
                                  << avg_pt_dist/(img.points.size() + 1e-6) << std::endl;
                        std::cout << ray_tree.value.size()
                            << " rays\n";
                        ++dirty[1];
                        if(draw_carved) {
                            update_verts();
                        }
                    });
                });//});
                ray_img = img_grab_out.try_pop();
                /* ray_img2 = img_grab_out.try_pop(); */
            }
        });

        Vec3f cameraDiff{{0,0,0}};

        if(mouseCaptured) {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
                cameraDiff[2] += -1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
                cameraDiff[2] += 1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
                cameraDiff[0] += -1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
                cameraDiff[0] += 1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
                cameraDiff[1] += -1;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
                cameraDiff[1] += 1;
            }
        }

        auto translation = camera.relativeTranslation(cameraDiff*CAM_SPEED*DT);
        camera.loc += translation;

        if(mouseCaptured) {
            sf::Vector2i mouseLoc2i = sf::Mouse::getPosition(window);
            Vec2i mouseCenter = {{ windowWidth /2,
                                   windowHeight-windowHeight/2 }};
            Vec2i mouseLoc = {{ mouseLoc2i.x,
                                windowHeight-mouseLoc2i.y }};
            mouseLoc -= mouseCenter;
            camera.heading += mouseLoc[0]*LOOK_SCALE;
            camera.pitch   += mouseLoc[1]*LOOK_SCALE;
            camera.constrain();

            sf::Mouse::setPosition(windowCenter,window);
        }

        /*std::cout << camera.loc << "\t" << viewDir << std::endl;*/
        if(just_draw_depth) {
            glBindFramebuffer(GL_FRAMEBUFFER,fbo);
        }

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        camera.begin();
        {
            size_t which = draw_carved;
            std::lock_guard<rw_mutex::mutex_wrapper>
                l(resize_lock[which].r_mutex());
            /* glScalef(10,10,10); */


            glDisableClientState(GL_COLOR_ARRAY);
            glEnableClientState(GL_VERTEX_ARRAY);


            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
            glEnableClientState(GL_COLOR_ARRAY);

            glColorPointer(3,GL_FLOAT,sizeof(Color3f),&colors[which][0][0]);
            glVertexPointer(3,GL_FLOAT,sizeof(Vec3f),&verts[which][0][0]);
            glDrawArrays(GL_TRIANGLES,0,draw_ind[which].load());

            glDisableClientState(GL_COLOR_ARRAY);

            /* glColor3f(1.0f,1.0f,1.0f); */
            /* glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); */

            /* glVertexPointer(3,GL_FLOAT,sizeof(Vec3f),&frame_verts[0][0]); */
            /* glDrawArrays(GL_TRIANGLES,0,frame_verts.size()); */

            glDisableClientState(GL_VERTEX_ARRAY);


        }
        for(size_t i=0; i < scanpoints.size(); ++i) {
            auto p = scanpoints[i];
            auto d = norm(scandirs[i]);
            auto right = norm(cross(Vec3f{{0,1,0}},scandirs[i]));
            auto up = cross(right,d);
            Mat44f m = {{right[0], right[1], right[2], 0, up[0],
                up[1], up[2], 0, d[0], d[1], d[2], 0, 0, 0, 0, 1}};

            glPushMatrix();
            /* gluLookAt(p[0],p[1],p[2],refpt[0],refpt[1],refpt[2],0,1,0); */
            glTranslatef(p[0],p[1],p[2]);
            glMultMatrixf(m.values);
            glColor3f(1,1,0);
            gluSphere(quadric,0.1,15,15);
            glColor3f(1,0,0);
            gluCylinder(quadric,0.1,0,0.2,15,15);
            glPopMatrix();
            /*if(i+1 >= scanpoints.size()) */
            {
                auto p2 = p + 20.0f*d;
                glBegin(GL_LINES);
                    glVertex3fv(&p[0]);
                    glVertex3fv(&p2[0]);
                glEnd();
            }
        }

        {
            auto right = norm(cross(Vec3f{{0,1,0}},user_dir));
            auto p = user_loc;// - space_origin;
            auto d = user_dir;
            auto up = cross(right,d);
            Mat44f m = {{right[0], right[1], right[2], 0, up[0],
                up[1], up[2], 0, d[0], d[1], d[2], 0, 0, 0, 0, 1}};

            glPushMatrix();
            /* gluLookAt(p[0],p[1],p[2],refpt[0],refpt[1],refpt[2],0,1,0); */
            glTranslatef(p[0], p[1], p[2]);
            glMultMatrixf(m.values);
            glColor3f(0,1,0);
            gluSphere(quadric,0.1,15,15);
            glColor3f(0,0.7,0);
            gluCylinder(quadric,0.1,0,0.2,15,15);
            glPopMatrix();
        }

        camera.end();

/*         scanpos.forward([](Vec3f pos) { */
/*             scandir.forward([pos](Vec3f dir) { */
/*                 glPushMatrix(GL_MODELVIEW); */

/*                     glTranslatef( */

/*                 glPopMatrix(GL_MODELVIEW); */
/*             }); */
/*         }); */
        if(just_draw_depth) {
            {
                pixels.resize(windowWidth*windowHeight);

                glReadPixels(0, 0, windowWidth, windowHeight,
                        GL_DEPTH_COMPONENT, GL_FLOAT, &pixels[0]);
                /* float minval,maxval,mean,var; */
                /* float minval = pixels[0]; */
                /* float maxval = pixels[0]; */
                /* float mean = pixels[0]/pixels.size(); */
                /* for(size_t i=1;i<pixels.size();++i) { */
                /*     mean += pixels[i]/pixels.size(); */
                /*     minval = std::min(minval,pixels[i]); */
                /*     maxval = std::max(maxval,pixels[i]); */
                /* } */
                /* float var = */
                /*     (pixels[0]-mean)*(pixels[0]-mean)/pixels.size(); */
                /* for(size_t i=1;i<pixels.size();++i) { */
                /*     var += */ 
                /*         (pixels[i]-mean)*(pixels[i]-mean)/pixels.size(); */
                /* } */

/*                 std::cout << " Max: " << maxval << std::endl; */
/*                 std::cout << " Min: " << minval << std::endl; */
/*                 std::cout << " Mean: " << mean << std::endl; */
/*                 std::cout << " Mean: " << mean << std::endl; */
/*                 std::cout << " Var:  " << var << std::endl; */

                newpixels.clear();
                newpixels.reserve(3*pixels.size());
                float n = camera.nearPlane;
                float f = camera.farPlane;
                for(auto z: pixels) {
                    z = -((n*f)/(-(z+(n+f)/(n-f))*(n-f)));
                    /* z = (2*n*f)/((n+f)/z + (n-f)); */
                    z -= n;
                    z /= (f-n);
                    /* x *= x; */
                    /* x *= x;//(maxval-minval + 1e-6); */
                    /* x *= x;//(maxval-minval + 1e-6); */
                    /* x *= x;//(maxval-minval + 1e-6); */
                    /* x *= x;//(maxval-minval + 1e-6); */
                    /* x *= x;//(maxval-minval + 1e-6); */
                    /* x *= x;//(maxval-minval + 1e-6); */
                    newpixels.push_back(z);
                    newpixels.push_back(z);
                    newpixels.push_back(z);
                }

                glBindTexture(GL_TEXTURE_2D,depth_texture);
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                        windowWidth, windowHeight, 0,
                        GL_RGB, GL_FLOAT, &newpixels[0]);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                        GL_LINEAR);
                glBindTexture(GL_TEXTURE_2D,0);

                /* minval = newpixels[0]; */
                /* maxval = newpixels[0]; */
                /* mean = newpixels[0]/newpixels.size(); */
                /* for(size_t i=1;i<newpixels.size();++i) { */
                /*     mean += newpixels[i]/newpixels.size(); */
                /*     minval = std::min(minval,newpixels[i]); */
                /*     maxval = std::max(maxval,newpixels[i]); */
                /* } */
                /* var = */
                /*     (newpixels[0]-mean)*(newpixels[0]-mean)/newpixels.size(); */
                /* for(size_t i=1;i<newpixels.size();++i) { */
                /*     var += */ 
                /*         (newpixels[i]-mean)*(newpixels[i]-mean)/newpixels.size(); */
                /* } */

                /* std::cout << " Max: " << maxval << std::endl; */
                /* std::cout << " Min: " << minval << std::endl; */
                /* std::cout << " Mean: " << mean << std::endl; */
                /* std::cout << " Mean: " << mean << std::endl; */
                /* std::cout << " Var:  " << var << std::endl; */
            }

            glBindFramebuffer(GL_FRAMEBUFFER,0);
        }

        glMatrixMode(GL_PROJECTION);

        glPushMatrix();
        glLoadIdentity();

        glMatrixMode(GL_MODELVIEW);

        if(!just_draw_depth) {
            glPointSize(2);
            glBegin(GL_POINTS);
                glColor3f(0,0,0);
                glVertex3f(0,0,0);
            glEnd();
        } else {
            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);

            glBindTexture(GL_TEXTURE_2D,depth_texture);
            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
            glBegin(GL_QUADS);
                glColor3f(1,1,1);
                glTexCoord2f(0,0);
                glVertex2f(-1,-1);
                glTexCoord2f(0,1);
                glVertex2f(-1,1);
                glTexCoord2f(1,1);
                glVertex2f(1,1);
                glTexCoord2f(1,0);
                glVertex2f(1,-1);
            glEnd();
            glFlush();

            glBindTexture(GL_TEXTURE_2D,0);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_CULL_FACE);
        }

        glMatrixMode(GL_PROJECTION);

        glPopMatrix();

        glMatrixMode(GL_MODELVIEW);

        auto timeForFrame = frameTime.getElapsedTime();
        float currFPS = 1.0/timeForFrame.asSeconds();
        if(timeForFrame.asSeconds() < DT) {
            sf::sleep(sf::seconds(DT)-timeForFrame);
            currFPS = FPS;
        }
        std::stringstream newTitle("");
        newTitle << TITLE << ": depth " <<
            tesselation_depth[(int)draw_carved] << ", "
                 << (int)(currFPS+0.5) << " FPS, "
                 << timeForFrame.asMilliseconds() << "/"
                 << TIME_PER_FRAME.asMilliseconds() << " ms/frame";
        window.setTitle(newTitle.str());

        window.display();
    }

    img_grab_poke.push(false);
    img_grab_poke.push(false);
    img_grab_poke.close();

    vive_loc_poke.push(false);
    vive_loc_poke.push(false);
    vive_loc_poke.close();

    server_thread.join();
    vive_loc_thread.join();

    done = true;
    update_notify_lock.lock();
    update_notify_lock.unlock();
    update_notify.notify_all();
    bg_updater.join();

    return 0;
}

