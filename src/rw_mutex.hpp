#ifndef RW_MUTEX_HPP
#define RW_MUTEX_HPP

#include <mutex>

class rw_mutex {
public:
    class mutex_wrapper {
        rw_mutex& m;
        const bool read;
        friend class rw_mutex;
        mutex_wrapper(rw_mutex& m_,bool read_) : m(m_),read(read_) {}

    public:
        mutex_wrapper()=delete;
        mutex_wrapper(const mutex_wrapper& other) :
            m(other.m),read(other.read) {}

        void lock() {
            if(read) {
                m.r_lock();
            } else {
                m.w_lock();
            }
        }

        void unlock() {
            if(read) {
                m.r_unlock();
            } else {
                m.w_unlock();
            }
        }
    };

private:

    std::mutex r,w;
    size_t readers = 0;
    mutex_wrapper r_wrapper,w_wrapper;

public:

    rw_mutex() : r_wrapper(*this,true),w_wrapper(*this,false) {}

    mutex_wrapper& r_mutex() {
        return r_wrapper;
    }

    mutex_wrapper& w_mutex() {
        return w_wrapper;
    }

    void r_lock() {
        r.lock();
        if(readers++ == 0) {
            w.lock();
        }
        r.unlock();
    }

    void r_unlock() {
        r.lock();
        if(--readers == 0) {
            w.unlock();
        }
        r.unlock();
    }

    void w_lock() {
        w.lock();
    }

    void w_unlock() {
        w.unlock();
    }
};

#endif

