#ifndef RAY_HPP
#define RAY_HPP

#include <limits>
#include <ostream>

struct Ray {
    Vec3f origin;
    Vec3f ray;

    bool operator==(const Ray& other) const {
        return other.origin == origin && other.ray == ray;
    }

    bool operator!=(const Ray& v) const {
        return !(*this == v);
    }

    bool intersects(const AABB& box) const {
        if(box.contains(origin)) {
            return true;
        }

        AABB fudge_box = box;
        fudge_box.size *= 1.001f;

        float tmin = -std::numeric_limits<float>::infinity();
        float tmax = std::numeric_limits<float>::infinity();
        /* float min_dist = std::numeric_limits<float>::infinity(); */
        for(size_t axis=0; axis < 3; ++axis) {
            if(ray[axis] == 0) { continue; }

            float dist_a = (box.center[axis]+0.5f*box.size[axis])
                           -origin[axis];
            float dist_b = (box.center[axis]-0.5f*box.size[axis])
                           -origin[axis];
            dist_a /= ray[axis];
            dist_b /= ray[axis];

            auto tfar   = std::max(dist_a,dist_b),
                 tclose = std::min(dist_a,dist_b);
            tmin = std::max(tmin,tclose);
            tmax = std::min(tmax,tfar);
            /* if(tclose >= 0) { */
/*                 min_dist = std::min(min_dist,tclose); */
            /* } */
            /* if(tfar >= 0) { */
                /* min_dist = std::min(min_dist,tfar); */
            /* } */
        }

        /* std::cout << " (tmin,tmax) = (" << tmin << "," << tmax */
        /*           << ")\n"; */
        /* std::cout << "  mindist = " << min_dist */
        /*           << "\n"; */
        return tmax >= tmin && tmin < 1 && tmin >= 0;// && tmin >= 0 && tmin <= 1;
    }
};

namespace std {
    template<>
    struct hash<Ray> {
        size_t operator()(const Ray& ray) const {
            hash<Vec3f> h;
            return h(ray.origin)+2*h(ray.ray);
        }
    };

    ostream& operator<<(ostream& o,const Ray& ray) {
        return o << "Ray{" << ray.origin << " -> "
                 << ray.ray << "}";
    }
}

#endif

