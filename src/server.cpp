/*!*
 * Simple chat program (server side).cpp - jchartou
 * Bassed off hassan yousef's server-code.
 */

#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include "server.hpp"

                    /*             Vec2f offset = */
                    /*                 Vec2f{{(float)x+0.5f,(float)(y+0.5f)}}-center; */
                    /*             offset[0] /= (windowWidth/2.0f); */
                    /*             offset[1] /= (windowHeight/2.0f); */
using namespace std;

// 2099
void vive_loc_server(int portNum, thread_queue<bool>& poke,
        thread_queue<std::pair<uint8_t,Mat44f>>& out) {
    signal(SIGPIPE,SIG_IGN);

    int client, server;
    constexpr int bufsize = 1024;
    uint8_t buffer[bufsize];

    struct sockaddr_in server_addr;
    socklen_t size;

    /* ---------- ESTABLISHING SOCKET CONNECTION ----------*/
    /* --------------- socket() function ------------------*/

    client = socket(AF_INET, SOCK_STREAM, 0);

    if (client < 0) 
    {
        cout << "\n[vive] Error establishing socket..." << endl;
        exit(1);
    }


    cout << "\n[vive] => Socket server has been created..." << endl;


    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(portNum);

    /* ---------- BINDING THE SOCKET ---------- */
    /* ---------------- bind() ---------------- */


    if ((bind(client, (struct sockaddr*)&server_addr,sizeof(server_addr))) < 0) 
    {
        cout << "[vive] => Error binding connection, the socket has already been established..." << endl;
        out.close();
        return;
    }


    size = sizeof(server_addr);

    /* ------------- LISTENING CALL ------------- */
    /* ---------------- listen() ---------------- */

    ssize_t err;
    err = listen(client, 1);
    cout << "[vive] Listening: " << err << std::endl;

    std::pair<uint8_t,Mat44f> ret;

    struct timeval tv = { 0 };
    tv.tv_sec = 2;
    tv.tv_usec = 0;

    setsockopt(client, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv,
            sizeof tv);
    setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,
            sizeof tv);

    int clientCount = 1;
    bool active = true;
    while(active) {
        poke.try_pop().forward([&active](bool x) { active = x; });
        if(!active || !(active = poke.is_open())) {
            break;
        }

        cout << "[vive] => Looking for clients..." << endl;

        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(client,&fds);
        struct timeval timeout = {0};
        timeout.tv_sec = 2;


        if(0 >= (err = select(client+1,&fds,NULL,NULL,&timeout))) {
            /* cout << "[vive] Select timed out: " << err << "\n"; */
            continue;
        }
        server = accept(client,(struct sockaddr *)&server_addr,&size);

        setsockopt(server, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv,
                sizeof tv);
        setsockopt(server, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,
                sizeof tv);

        // first check if it is valid or not
        if (server < 0) {
            cout << "[vive] => Error on accepting..." << endl;
            continue;
        }


        /* strcpy(buffer, "=> Server connected...\n"); */
        /* send(server, buffer, bufsize, 0); */
        cout << "[vive] => Connected with the client #" << clientCount++ << ", you are good to go..." << endl;

        cout << "[vive] Client: \n";
        *buffer=' ';

        cout << "[vive] Waiting for request...\n";
        while(active) {
            active = poke.try_pop().get(true);
            if(!active) { break; }
            active = poke.is_open();
            if(!active) { break; }

            /* float vals[16] = { 0 }; */
            float* vals = &ret.second.values[0];
            size_t start = 0;
            do {
                if(0 >= (err = recv(server, (uint8_t*)&vals[0] +
                                start,
                                (16*sizeof(float)-start), 0))) {
                    if(err < 0) {
                        err = errno;
                    }
                    if(err != EAGAIN && err != EWOULDBLOCK) {
                        cout << "[vive] Recv failed: " << err << " / "
                             << errno << ": " << strerror(errno) << "\n";
                    }
                } else {
                    start += err;
                }
            } while(start < 16*sizeof(float) && err > 0);
            uint8_t trigger = 0;
            if(err > 0) {
                err = recv(server,&trigger,sizeof(uint8_t),0);
            }

            if(err <= 0 && err != EAGAIN && err != EWOULDBLOCK) {
                break;
            }
            if(start >= 16*sizeof(float)) {
                /* std::cout << "[vive] received "; */
                /* for(size_t i=0; i < 16; ++i) { */
                /*     if(i > 0) { */
                /*         std::cout << ", "; */
                /*     } */

                /*     std::cout << vals[i]; */
                /* } */
                /* std::cout << "\n"; */

                ret.first = trigger;
                /* ret = Mat44f::translation(vals[0],vals[1],vals[2])* */
                /*       Mat44f::rotation(vals[3],1,0,0)* */
                /*       Mat44f::rotation(vals[4],0,1,0)* */
                /*       Mat44f::rotation(vals[5],0,0,1); */
                out.push(ret);
            }
        }

        cout << "[vive] done: " << err << "\n";
        close(server);
    }

    close(client);
    out.close();
}


void img_grab_server(int portNum, thread_queue<bool>& poke,
        thread_queue<realsense_depth_img>& out)
{

    signal(SIGPIPE,SIG_IGN);

    int client, server;
    constexpr int bufsize = 1024;
    uint8_t buffer[bufsize];

    realsense_depth_img ret;

    struct sockaddr_in server_addr;
    socklen_t size;

    /* ---------- ESTABLISHING SOCKET CONNECTION ----------*/
    /* --------------- socket() function ------------------*/

    client = socket(AF_INET, SOCK_STREAM, 0);

    if (client < 0) 
    {
        cout << "\n[realsense] Error establishing socket..." << endl;
        exit(1);
    }


    cout << "\n[realsense] => Socket server has been created..." << endl;


    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(portNum);

    /* ---------- BINDING THE SOCKET ---------- */
    /* ---------------- bind() ---------------- */


    if ((bind(client, (struct sockaddr*)&server_addr,sizeof(server_addr))) < 0) 
    {
        cout << "[realsense] => Error binding connection, the socket has already been established..." << endl;
        out.close();
        return;
    }


    size = sizeof(server_addr);

    /* ------------- LISTENING CALL ------------- */
    /* ---------------- listen() ---------------- */

    ssize_t err;
    err = listen(client, 1);
    cout << "[realsense] Listening: " << err << std::endl;

    struct timeval tv = { 0 };
    tv.tv_sec = 2;
    tv.tv_usec = 0;

    setsockopt(client, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv,
            sizeof tv);
    setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,
            sizeof tv);

    int clientCount = 1;
    bool active = true;
    while(active) {
        poke.try_pop().forward([&active](bool x) { active = x; });
        if(!active || !(active = poke.is_open())) {
            break;
        }

        cout << "[realsense] => Looking for clients..." << endl;

        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(client,&fds);
        struct timeval timeout = {0};
        timeout.tv_sec = 2;


        if(0 >= (err = select(client+1,&fds,NULL,NULL,&timeout))) {
            /* cout << "[realsense] Select timed out: " << err << "\n"; */
            continue;
        }
        server = accept(client,(struct sockaddr *)&server_addr,&size);

        setsockopt(server, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv,
                sizeof tv);
        setsockopt(server, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,
                sizeof tv);

        // first check if it is valid or not
        if (server < 0) {
            cout << "[realsense] => Error on accepting..." << endl;
            continue;
        }


        /* strcpy(buffer, "=> Server connected...\n"); */
        /* send(server, buffer, bufsize, 0); */
        cout << "[realsense] => Connected with the client #" << clientCount++ << ", you are good to go..." << endl;

        cout << "[realsense] Client: \n";
        *buffer=' ';

        Maybe<bool> poke_val;
        while(active) {
            poke_val = poke.pop(2000.0);
            active = poke_val.get(true);
            if(!active) { break; }
            active = poke.is_open();
            if(!active) { break; }

            if(!poke_val) {
                /* cout << "[realsense] Poke timed out...\n"; */
                *buffer = ' ';
                if(0 >= (err = send(server,buffer,1,0))) {
                    cout << "[realsense] Heartbeat failed\n";
                    break;
                }
                continue;
            }

            *buffer = 'i';
            cout << "[realsense] Sending request... ('" << *buffer << "')\n";
            if(0 >= (err = send(server,buffer,1,0))) {
                cout << "[realsense] Send failed\n";
                break;
            }
            if(0 >= (err = recv(server, buffer, 1, 0))) {
                cout << "[realsense] Recv failed\n";
                break;
            }
            if(0 >= (err = send(server, buffer, 1, 0))) {
                cout << "[realsense] resend failed\n";
                break;
            }

            cout << "[realsense] Got '" << *buffer << "'\n";

            if (*buffer == 's') {

                if(send(server,buffer,1,0) < 1) { break; }
                if(recv(server,buffer,8,0) < 4) { break; }
                if(recv(server,ret.angles,sizeof(ret.angles),0) <
                        sizeof(ret.angles)) { break; }
                size_t w = (size_t)buffer[0] |
                    (((size_t)buffer[1])<<8)  |
                    (((size_t)buffer[2])<<16) |
                    (((size_t)buffer[3])<<24);
                size_t h = (size_t)buffer[4] |
                    (((size_t)buffer[5])<<8)  |
                    (((size_t)buffer[6])<<16) |
                    (((size_t)buffer[7])<<24);

                cout << "[realsense] New size: " << w*h*3 << "\n";


                ret.w = w;
                ret.h = h;
                ret.pixels.resize(w*h*3);

                size_t start = 0;
                while(start < ret.pixels.size()) {
                    err = recv(server,&ret.pixels[0]+start,ret.pixels.size()-start,0);
                    if(err <= 0) { break; }
                    start += err;
                }
                if(start < ret.pixels.size()) {
                    cout << "[realsense] Img recv failed: only got " << start
                         << " bytes\n";
                    break;
                }

                size_t num_points;
                if(0 >= recv(server, &num_points, sizeof(num_points),
                            0)) {
                    cout << "[realsense] num_points recv failed: "
                         << strerror(errno) << std::endl;
                    break;
                }

                std::cout << "Receiving " << num_points << " points\n";

                ret.points.resize(num_points/3);
                start = 0;
                size_t bytes_recv = ret.points.size()*3*sizeof(float);
                while(start < bytes_recv) {
                    err = recv(server,
                            ((uint8_t*)&ret.points[0][0])+start,
                            bytes_recv-start, 0);
                    if(err <= 0) { break; }
                    start += err;
                }
                if(start < bytes_recv) {
                    cout << "[realsense] Points recv failed: only got "
                         << start << " bytes of " <<
                         bytes_recv << "\n";
                    break;
                }

                cout << "[realsense] Received img. Sending...\n";
                out.push(ret);
            } else { break; }
        }

        cout << "[realsense] done: " << err << "\n";
        close(server);
    }

    close(client);
    out.close();
}
