#ifndef MESHTRIANGLE_HPP
#define MESHTRIANGLE_HPP

struct MeshTriangle {
    Triangle3D tri;
    Color3D color;
}

#endif

