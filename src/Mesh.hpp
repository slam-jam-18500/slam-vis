#ifndef MESH_HPP
#define MESH_HPP

#include "Triangle.hpp"
#include <vector>

struct Mesh {
    std::vector<Triangle3D> tris;
};

#endif

