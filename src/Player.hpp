#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Entity.hpp"
#include "Camera.hpp"

class Player : public Entity {
    Camera _cam;

protected:

    void _update(float dt) {
        _cam.loc = _loc;
    }
};

#endif
