#ifndef OCTTREE_HPP
#define OCTTREE_HPP

#include "Vector.hpp"
#include "AABB.hpp"
#include "tiny_obj_loader.h"
#include <memory>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <cassert>
#include <atomic>
#include <iostream>

template<class T>
struct OctTree {
    AABB box;
    std::unique_ptr<OctTree> subtrees[2][2][2];
    bool tesselated = false;

    T value;

    AABB sub_box(size_t x,size_t y, size_t z) const {
        assert(x < 2 && y < 2 && z < 2);

        AABB ret;

        int sx = 2*(int)x - 1,
            sy = 2*(int)y - 1,
            sz = 2*(int)z - 1;
        Vec3f delta = {{
            box.size[0]/4 * sx,
            box.size[1]/4 * sy,
            box.size[2]/4 * sz,
        }};

        ret.size = (1.001f*0.5f)*(box.size);
        ret.center = box.center + delta;

        return ret;
    }

    void clear() {
        value.clear();
        for(size_t x = 0; x < 2; ++x) {
            for(size_t y = 0; y < 2; ++y) {
                for(size_t z = 0; z < 2; ++z) {
                    subtrees[x][y][z].reset();
                }
            }
        }
        tesselated = false;
    }
};

// requires that T has a method bool T::intersects(AABB) const
// (or compatible)
template<class T>
void tesselate_layer(OctTree<std::unordered_set<T>>& tree) {
    std::vector<AABB> boxes;
    AABB union_box;
    bool has_child = false;
    for(size_t x = 0; x < 2; ++x) {
        for(size_t y = 0; y < 2; ++y) {
            for(size_t z = 0; z < 2; ++z) {
                AABB box = tree.sub_box(x,y,z);

                assert(box.size[0] >= 0);
                assert(box.size[1] >= 0);
                assert(box.size[2] >= 0);

                /* box.center = (minbound + (maxbound-minbound)/2); */
                /* std::cout << "(" << x << "," << y << "," << z */
                /*             << ")/(" << sx << "," << sy << "," << sz */
                /*             << "): " << delta << std::endl; */
                /* std::cout << "\tcenter: " << box.center */
                /*             << "\n\tsize: " << box.size */
                /*             << std::endl; */

                if(tree.subtrees[x][y][z]) {
                    has_child = true;
                    /* box = tree.subtrees[x][y][z]->box; */
                    assert(!tree.subtrees[x][y][z]->value.empty());
                    assert(tree.subtrees[x][y][z]->box == box);
                }
                AABB fudged_outer_box = tree.box;
                fudged_outer_box.size *= 1.2;
                assert(fudged_outer_box.contains(box));

                for(auto b: boxes) {
                    assert(b.checkCollision(box));
                    assert(box.checkCollision(b));
                }
                if(boxes.empty()) { union_box = box; }

                boxes.push_back(box);

                {
                    Vec3f union_min = union_box.center -
                        union_box.size/2;
                    Vec3f union_max = union_box.center +
                        union_box.size/2;
                    Vec3f box_min = box.center - box.size/2;
                    Vec3f box_max = box.center + box.size/2;

                    for(size_t i=0; i < 3; ++i) {
                        if(union_min[i] > union_max[i]) {
                            std::swap(union_min[i],union_max[i]);
                        }
                        if(box_min[i] > box_max[i]) {
                            std::swap(box_min[i],box_max[i]);
                        }
                        union_min[i] =
                            std::min(union_min[i],box_min[i]);
                        union_max[i] =
                            std::max(union_max[i],box_max[i]);
                    }
                    union_box.center = (union_min/2+union_max/2);
                    union_box.size = 1.0001f*(union_max-union_min);

                    AABB overall_box = tree.box;
                    overall_box.size *= 1.2f;

                    assert(union_box.contains(box));
                    assert(overall_box.contains(union_box));
                }

                assert(box.checkCollision(tree.box,false));

                auto& subtree = tree.subtrees[x][y][z];

                /* size_t i = 0; */
                for(const auto& v: tree.value) {
                    /* std::cout << "Checking entry " << (i++) */
                    /*           << " (" << v << ")" << std::endl; */

                    assert(v.intersects(tree.box));
                    assert(tree.box.checkCollision(box,false));

#if 0
                    int sx = 2*(int)x - 1,
                        sy = 2*(int)y - 1,
                        sz = 2*(int)z - 1;
                    for(size_t j = 0; j < 3; ++j) {
                        auto diff = v.verts[j]-tree.box.center;
                        if(diff[0]*sx >= 0 && diff[1]*sy >= 0 &&
                           diff[2]*sz >= 0) {
                            /* std::cout << "  (" << i << "," << j */
                            /*     << ") is in (" << sx */
                            /*     << "," << sy << "," << sz << ")\n"; */
                            /* std::cout << "  " << v.verts[j] << "\n"; */
                            /* std::cout << "  " << diff << "\n"; */
                            /* std::cout << " Tree-Box-center: " << tree.box.center */
                            /*     << "\n Tree-Box-size: " << tree.box.size */
                            /*     << std::endl; */
                            /* std::cout << " Box-center: " << box.center */
                            /*     << "\n Box-size: " << box.size */
                            /*     << std::endl; */
                            if(tree.box.contains(v.verts[j])) {
                                assert(box.contains(v.verts[j]));
                                assert(v.intersects(box));
                            }
                        }
                    }
#endif

                    if(v.intersects(box)) {
                        /* std::cout << "\tHit!" << std::endl; */
                        if(!subtree) {
                            subtree.reset(
                                new OctTree<std::unordered_set<T>>);
                            subtree->box = box;
                            assert(subtree->value.empty());
                            has_child = true;
                        }
                        assert(tree.subtrees[x][y][z]);
                        assert(subtree);

                        if(!subtree->value.count(v)) {
                            subtree->tesselated = false;
                        }
                        subtree->value.insert(v);
                        assert(!subtree->value.empty());
                        assert(!tree.subtrees[x][y][z]->value.empty());
                    }
                }
            }
        }
    }

    tree.tesselated = true;

    assert(union_box.contains(tree.box));
    assert(boxes.size() == 8);

#ifndef NDEBUG
    for(size_t i = 0; i < boxes.size(); ++i) {
        AABB a = boxes[i];
        Vec3f a_min = a.center - a.size/2;
        Vec3f a_max = a.center + a.size/2;
        for(size_t j = 0; j < boxes.size(); ++j) {
            if(i == j) { continue; }

            AABB b = boxes[j];
            Vec3f b_min = b.center - b.size/2;
            Vec3f b_max = b.center + b.size/2;

            Vec3f ab_min = a_min;
            Vec3f ab_max = a_max;

            ab_min[0] = std::min(ab_min[0],b_min[0]);
            ab_min[1] = std::min(ab_min[1],b_min[1]);
            ab_min[2] = std::min(ab_min[2],b_min[2]);

            ab_max[0] = std::max(ab_max[0],b_max[0]);
            ab_max[1] = std::max(ab_max[1],b_max[1]);
            ab_max[2] = std::max(ab_max[2],b_max[2]);

            AABB ab_union = { (ab_min/2 + ab_max/2),
                1.01f*(ab_max-ab_min) };

            for(size_t k = 0; k < boxes.size(); ++k) {
                if(!(((i^j)|(i^k)|(j^k)) == 7)) {
                    continue;
                }

                AABB c = boxes[k];

                if(ab_union.contains(c)) {
                    continue;
                }

                /* std::cout << "\n\n Unioning " << i << "," << j << "," */
                /*           << k << std::endl; */

                Vec3f c_min = c.center - c.size/2;
                Vec3f c_max = c.center + c.size/2;

                Vec3f min_bound = a_min;
                Vec3f max_bound = a_max;

                min_bound[0] = std::min(ab_min[0],c_min[0]);
                max_bound[0] = std::max(ab_max[0],c_max[0]);

                min_bound[1] = std::min(ab_min[1],c_min[1]);
                max_bound[1] = std::max(ab_max[1],c_max[1]);

                min_bound[2] = std::min(ab_min[2],c_min[2]);
                max_bound[2] = std::max(ab_max[2],c_max[2]);

                AABB abc_union = { (min_bound/2 + max_bound/2),
                    1.00001f*(max_bound-min_bound) };

/*                 std::cout <<   "  A center: " */
/*                           << a.center */
/*                           << "\n  A size: " */
/*                           << a.size << std::endl; */
/*                 std::cout <<   "  A min: " */
/*                           << a_min */
/*                           << "\n  A max: " */
/*                           << a_max << std::endl; */

/*                 std::cout <<   "  B center: " */
/*                           << b.center */
/*                           << "\n  B size: " */
/*                           << b.size << std::endl; */
/*                 std::cout <<   "  B min: " */
/*                           << b_min */
/*                           << "\n  B max: " */
/*                           << b_max << std::endl; */

/*                 std::cout <<   "  C center: " */
/*                           << c.center */
/*                           << "\n  C size: " */
/*                           << c.size << std::endl; */
/*                 std::cout <<   "  C min: " */
/*                           << c_min */
/*                           << "\n  C max: " */
/*                           << c_max << std::endl; */

/*                 std::cout <<   "  min: " */
/*                           << min_bound */
/*                           << "\n  max: " */
/*                           << max_bound << std::endl; */

/*                 std::cout <<   "  ABC-union center: " */
/*                           << abc_union.center */
/*                           << "\n  ABC-union size: " */
/*                           << abc_union.size << std::endl; */
/*                 std::cout <<   "  Tree center: " */
/*                           << tree.box.center */
/*                           << "\n  Tree size: " */
/*                           << tree.box.size << std::endl; */

                assert(abc_union.contains(a));
                assert(abc_union.contains(b));
                assert(abc_union.contains(c));
                assert(abc_union.contains(tree.box));
                assert(union_box.contains(abc_union));
                abc_union.size *= 1.1f;
                assert(abc_union.contains(union_box));

            }
        }
    }


    for(auto v: tree.value) {
        assert(v.intersects(tree.box));
        assert(union_box.contains(tree.box));
        assert(v.intersects(union_box));

        /* std::cout << "  " << v << "\n"; */

        bool in_child = false;
        for(size_t x=0; x < 2; ++x) {
            for(size_t y=0; y < 2; ++y) {
                for(size_t z=0; z < 2; ++z) {
                    if(tree.subtrees[x][y][z]) {
                        /* std::cout << "   Checking (" << x << "," << y */
                        /*     << "," << z << ")\n"; */

                        auto& subtree = *tree.subtrees[x][y][z];

                        /* std::cout << "   Center: " << */
                        /*     subtree.box.center << std::endl; */
                        /* std::cout << "   Size: " << */
                        /*     subtree.box.size << std::endl; */

                        if(v.intersects(subtree.box)) {
                            assert(subtree.value.count(v));
                            in_child = true;
                            /* std::cout << "  Hit!\n"; */
                            break;
                        }
                    } else {

                        AABB box = boxes[4*x + 2*y + z];

                        assert(!v.intersects(box));

                    }
                }
            }
        }
        assert(in_child);
    }
    assert(has_child == !tree.value.empty());

#endif

}

template<class T>
void for_each_leaf(OctTree<std::unordered_set<T>>& tree,
        size_t tesselation_depth,
        std::function<void(const OctTree<std::unordered_set<T>>&)> f,
        Maybe<std::function<void(const AABB&)>>
            on_empty = {},
        Maybe<std::function<void(std::function<void()>)>> run = {}) {

#ifndef NDEBUG
    /* std::cout << "Tesselating: max depth = " << tesselation_depth */
    /*     << std::endl; */
#endif
    if(tree.value.empty()) {
        on_empty.forward([&tree](std::function<void(const AABB&)> f) {
                /* std::cout << "Running on_empty on " << tree.box.center */
                /*           << ", " << tree.box.size << std::endl; */
                f(tree.box);
            });
        return;
    }
    if(tesselation_depth == 0) {
        f(tree);
        return;
    }

    bool has_children = false;
    size_t i = 0;

    while(!has_children || !tree.tesselated) {
        if(i > 0 || !tree.tesselated) {
            tesselate_layer(tree);
        }

        if(i >= 2) {
            assert(tree.value.empty());
            /* f(tree); */
            on_empty.forward([&tree](std::function<void(const AABB&)> f) {
                    /* std::cout << "Running on_empty on " << tree.sub_box(x,y,z).center */
                    /*         << ", " << tree.sub_box(x,y,z).size << std::endl; */
                    f(tree.box);
                });
            return;
        }
        assert(i < 2);

        for(size_t x = 0; x < 2; ++x) {
            for(size_t y = 0; y < 2; ++y) {
                for(size_t z = 0; z < 2; ++z) {
                    if(tree.subtrees[x][y][z]) {
                        has_children = true;
                        auto* tree_ptr = &tree;
                        auto fn =
                                [tree_ptr,x,y,z,tesselation_depth,f,on_empty,run]() {
                            for_each_leaf(*tree_ptr->subtrees[x][y][z],
                                        tesselation_depth-1,f,on_empty,run);
                        };
                        if(!run) {
                            fn();
                        } else {
                            run.forward([&fn](std::function<void(std::function<void()>)>
                                        r) {
                                r(fn);
                            });
                        }
                    } else if(tree.tesselated) {
                        on_empty.forward([&tree,&x,&y,&z](std::function<void(const AABB&)> f) {
                                /* std::cout << "Running on_empty on " << tree.sub_box(x,y,z).center */
                                /*         << ", " << tree.sub_box(x,y,z).size << std::endl; */
                                f(tree.sub_box(x,y,z));
                            });
                    }
                }
            }
        }

        ++i;
    }
}


template<class T>
void minimize_tree(OctTree<std::unordered_set<T>>& tree,
        size_t tesselation_depth,
        Maybe<std::function<void(std::function<void()>)>> run = {},
        Maybe<std::function<void()>> barrier = {}) {

    std::unordered_map<T,std::atomic<size_t>> required_by;
    for(auto& x: tree.value) {
        required_by[x].store(0);
    }
    std::atomic<size_t> num_nodes(0);

    std::function<void (const OctTree<std::unordered_set<T>>&)>
        get_counts = [&required_by,&num_nodes](const
                OctTree<std::unordered_set<T>>& level) {
        ++num_nodes;
        for(auto& x: level.value) {
            if(required_by.count(x)) {
                ++required_by.at(x);
            }
        }
    };
    for_each_leaf(tree,tesselation_depth, get_counts,{},run);
    barrier.forward(std::function<void(std::function<void()>)>([](std::function<void()>
                    b) { b(); }));

    const auto& const_required_by = required_by;

    std::vector<T> required_list;
    required_list.resize(num_nodes.load());
    num_nodes.store(0);

    std::function<void (const OctTree<std::unordered_set<T>>&)>
        greedy_cover = [&const_required_by,&required_list,&num_nodes](const
                OctTree<std::unordered_set<T>>& level) {
        size_t max_count = 0;
        T rep;
        for(auto& x: level.value) {
            if(!const_required_by.count(x)) {
                required_list[num_nodes++] = x;
                continue;
            }
            size_t count = const_required_by.at(x);
            if(count > max_count) {
                max_count = count;
                rep = x;
            }
        }
        required_list[num_nodes++] = rep;
    };
    for_each_leaf(tree,tesselation_depth, greedy_cover,{},run);
    barrier.forward([](std::function<void()> b) { b(); });

    std::unordered_set<T>
        required(required_list.begin(),required_list.begin() +
                num_nodes.load());

    if(tree.value != required) {
        tree.value = std::move(required);
        for(size_t x = 0; x < 2; ++x) {
            for(size_t y = 0; y < 2; ++y) {
                for(size_t z = 0; z < 2; ++z) {
                    tree.subtrees[x][y][z].reset();
                }
            }
        }

        std::function<void (const OctTree<std::unordered_set<T>>&)>
            noop = [](const OctTree<std::unordered_set<T>>&) {};
        for_each_leaf(tree,tesselation_depth, noop,{},run);
    }
}

#endif

