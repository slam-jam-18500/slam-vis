#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "AABB.hpp"

struct Triangle2D {
    Vec2f verts[3];

    bool intersects(const Triangle2D& other) const {
        /* Vec2f points[] = { */
        /*     verts[0], */
        /*     verts[1], */
        /*     verts[2], */
        /*     other.verts[0], */
        /*     other.verts[1], */
        /*     other.verts[2] */
        /* }; */
        Vec2f deltas[] = {
            verts[1]-verts[0],
            verts[2]-verts[1],
            verts[0]-verts[2],
            other.verts[1]-other.verts[0],
            other.verts[2]-other.verts[1],
            other.verts[0]-other.verts[2]
        };

        for(size_t i = 0; i < 6; ++i) {
            deltas[i][0] *= -1;
            std::swap(deltas[i][0],deltas[i][1]);
            float coords[] = {
                dot(deltas[i],verts[0]),//-points[i]),
                dot(deltas[i],verts[1]),//-points[i]),
                dot(deltas[i],verts[2])//-points[i])
            };

            float other_coords[] = {
                dot(deltas[i],other.verts[0]),//-points[i]),
                dot(deltas[i],other.verts[1]),//-points[i]),
                dot(deltas[i],other.verts[2])//-points[i])
            };

            float min_coord = std::min(coords[0],
                    std::min(coords[1],coords[2]));
            float max_coord = std::max(coords[0],
                    std::max(coords[1],coords[2]));
            float other_min_coord = std::min(other_coords[0],
                    std::min(other_coords[1],other_coords[2]));
            float other_max_coord = std::max(other_coords[0],
                    std::max(other_coords[1],other_coords[2]));

            if(min_coord > other_max_coord) { return false; }
            if(other_min_coord > max_coord) { return false; }
        }

        return true;
    }
};

struct Triangle3D {
    Vec3f verts[3];

    bool operator==(const Triangle3D& other) const {
        for(size_t i = 0; i < 3; ++i) {
            if(verts[i] != other.verts[i]) {
                return false;
            }
        }
        return true;
    }

    bool operator!=(const Triangle3D& v) const {
        return !(*this == v);
    }

    bool intersects(const Triangle3D& other) const {

        /* Vec3f minbounds = verts[0], */
        /*       maxbounds = verts[0]; */
        /* Vec3f other_minbounds = other.verts[0], */
        /*       other_maxbounds = other.verts[0]; */
        /* for(size_t i = 0; i < 3; ++i) { */
        /*     minbounds[i] = std::min(minbounds[i],verts[1][i]); */
        /*     minbounds[i] = std::min(minbounds[i],verts[2][i]); */

        /*     maxbounds[i] = std::max(maxbounds[i],verts[1][i]); */
        /*     maxbounds[i] = std::max(maxbounds[i],verts[2][i]); */

        /*     other_minbounds[i] = std::min(other_minbounds[i],other.verts[1][i]); */
        /*     other_minbounds[i] = std::min(other_minbounds[i],other.verts[2][i]); */

        /*     other_maxbounds[i] = */
        /*         std::max(other_maxbounds[i],other.verts[1][i]); */
        /*     other_maxbounds[i] = std::max(other_maxbounds[i],other.verts[2][i]); */
        /* } */

        /* AABB mybb = { (minbounds+(maxbounds-minbounds)/2), */
        /*     (maxbounds-minbounds) }; */
        /* AABB otherbb = { */
        /*     (other_minbounds + (other_maxbounds-other_minbounds)/2), */
        /*     (other_maxbounds-other_minbounds) }; */

        /* if(!mybb.checkCollision(otherbb,false)) { */
        /*     return false; */
        /* } */

        Vec3f norm = cross(verts[1]-verts[0],verts[2]-verts[0]);
        Vec3f other_norm = cross(other.verts[1]-other.verts[0],
                                 other.verts[2]-other.verts[0]);

        float other_base = dot(other.verts[0],other_norm);
        float coords[] = {
            dot(verts[0],other_norm),
            dot(verts[1],other_norm),
            dot(verts[2],other_norm)
        };
        if((coords[0] > other_base && coords[1] > other_base
                               && coords[2] > other_base) ||
           (coords[0] < other_base && coords[1] < other_base
                               && coords[2] < other_base)) {
            return false;
        }

        float base_coord = dot(verts[0],norm);
        float other_coords[] = {
            dot(other.verts[0],norm),
            dot(other.verts[1],norm),
            dot(other.verts[2],norm)
        };

        if((other_coords[0] > base_coord && other_coords[1] > base_coord
                               && other_coords[2] > base_coord) ||
           (other_coords[0] < base_coord && other_coords[1] < base_coord
                               && other_coords[2] < base_coord)) {
            return false;
        }

        Vec3f x_axis = verts[1]-verts[0],
              y_axis = verts[2]-verts[0];//cross(x_axis,cross(verts[2]-verts[0],x_axis));
        x_axis /= x_axis.mag();
        y_axis /= y_axis.mag();

        Triangle2D projected,other_projected;
        for(size_t i = 0; i < 3; ++i) {
            projected.verts[i][0] = dot(verts[i],x_axis);
            projected.verts[i][1] = dot(verts[i],y_axis);
            other_projected.verts[i][0] = dot(other.verts[i],x_axis);
            other_projected.verts[i][1] = dot(other.verts[i],y_axis);
        }

        return projected.intersects(other_projected);
    }

    bool intersects(const AABB& box) const {

        assert(box.size[0] >= 0 && box.size[1] >= 0
                                && box.size[2] >= 0);

/*         Vec3f minbounds = verts[0], */
/*               maxbounds = verts[0]; */
/*         for(size_t i = 0; i < 3; ++i) { */
/*             minbounds[i] = std::min(minbounds[i],verts[1][i]); */
/*             minbounds[i] = std::min(minbounds[i],verts[2][i]); */

/*             maxbounds[i] = std::max(maxbounds[i],verts[1][i]); */
/*             maxbounds[i] = std::max(maxbounds[i],verts[2][i]); */
/*         } */

/*         AABB mybb = { (minbounds/2+maxbounds/2), */
/*             (maxbounds-minbounds) }; */

/*         if(!mybb.checkCollision(box,false)) { */
/*             return false; */
/*         } */

/*         if(box.contains(verts[0]) || box.contains(verts[1]) || */
/*            box.contains(verts[2])) { */
/*             return true; */
/*         } */

        Vec3f corners[8];
        for(size_t x = 0; x < 2; ++x) {
            for(size_t y = 0; y < 2; ++y) {
                for(size_t z = 0; z < 2; ++z) {
                    corners[x*4+y*2+z] = box.center +
                        Vec3f{{
                            (box.size[0]/2)*(2*(int)x - 1),
                            (box.size[1]/2)*(2*(int)y - 1),
                            (box.size[2]/2)*(2*(int)z - 1)}};
                }
            }
        }

        // search for separating AABB axis
        for(size_t i = 0; i < 3; ++i) {
            float max_coord = std::max(verts[0][i],
                    std::max(verts[1][i],verts[2][i]));
            float min_coord = std::min(verts[0][i],
                    std::min(verts[1][i],verts[2][i]));
            if(box.center[i] + box.size[i]/2 < min_coord) {
                /* std::cout << "[intersects] Below min on axis " << i << std::endl; */
                return false;
            }
            if(max_coord + box.size[i]/2 < box.center[i] ) {
                /* std::cout << "[intersects] Above max on axis " << i << std::endl; */
                return false;
            }
        }

        // triangle's plane
        Vec3f normal =
            norm(cross(verts[1]-verts[0],verts[2]-verts[0]));
        {
            float base_coord = dot(verts[0],normal);
            float min_coord = dot(corners[0],normal);
            float max_coord = min_coord;
            for(size_t i = 1; i < 8; ++i) {
                float coord = dot(corners[i],normal);
                min_coord = std::min(min_coord,coord);
                max_coord = std::max(max_coord,coord);
            }
            assert(min_coord <= max_coord);

            if(min_coord > base_coord) {
                /* std::cout */
                /*  << "[intersects] min box coord above plane on axis " */
                /*  << normal << std::endl; */
                return false;
            }
            if(max_coord < base_coord) {
                /* std::cout */
                /*  << "[intersects] max box coord below plane on axis " */
                /*  << normal << std::endl; */
                return false;
            }
        }

        // separating triangle axes
        for(size_t i = 0; i < 3; ++i) {
            size_t j = (i+1)%3;
            for(size_t v = 0; v < 3; ++v) {
                Vec3f aabb_axis = {{0,0,0}};
                aabb_axis[v] = 1;
                Vec3f axis = norm(cross(aabb_axis,
                            verts[j]-verts[i]));

                float min_coord = std::min(dot(verts[0],axis),
                        std::min(dot(verts[1],axis),dot(verts[2],axis)));
                float max_coord = std::max(dot(verts[0],axis),
                        std::max(dot(verts[1],axis),dot(verts[2],axis)));

                float max_box_coord = dot(corners[0],axis);
                float min_box_coord = max_box_coord;
                for(size_t k = 1; k < 8; ++k) {
                    float coord = dot(corners[k],axis);
                    max_box_coord = std::max(coord,max_box_coord);
                    min_box_coord = std::min(coord,min_box_coord);
                }

                if(max_box_coord < min_coord) {
                    /* std::cout */
                    /*  << "[intersects] max box coord (" << max_box_coord */
                    /*  << ") below min (" << min_coord << ") on tri-axis " */
                    /*  << i << ": " << axis << std::endl; */
                    return false;
                }
                if(max_coord < min_box_coord) {
                    /* std::cout */
                    /*  << "[intersects] min box coord (" << min_box_coord */
                    /*  << ") above max (" << max_coord << ") on tri-axis " */
                    /*  << i << ": " << axis << std::endl; */
                    return false;
                }
            }
        }

        return true;
    }
};

namespace std {
    template<>
    struct hash<Triangle3D> {
        size_t operator()(const Triangle3D& tri) const {
            hash<Vec3f> h;
            return h(tri.verts[0])+2*h(tri.verts[1])+3*h(tri.verts[2]);
        }
    };

    ostream& operator<<(ostream& o,const Triangle3D& tri) {
        return o << "Triangle3D{" << tri.verts[0] << ","
                 << tri.verts[1] << "," << tri.verts[2] << "}";
    }
}

#endif

