#ifndef THREAD_QUEUE_HPP
#define THREAD_QUEUE_HPP

#include <queue>
#include <chrono>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "Maybe.hpp"

template<class T>
class thread_queue {
    std::queue<T> q;
    std::mutex lock;
    std::condition_variable notify;
    std::atomic<size_t> queue_size;
    bool open;

public:
    thread_queue() {
        open = true;
        queue_size.store(0);
    }

    void push(T x) {
        if(!open) { return; }
        std::lock_guard<std::mutex> l(lock);
        q.push(x);
        ++queue_size;
        notify.notify_all();
    }

    bool is_open() const { return open; }

    Maybe<T> try_pop() {
        if(!open || !queue_size.load()) {
            return {};
        }
        {
            std::lock_guard<std::mutex> l(lock);
            if(!queue_size.load()) { return {}; }

            Maybe<T> ret(q.front());
            q.pop();
            --queue_size;

            return ret;
        }
    }

    Maybe<T> pop(Maybe<double> timeout = {}) {
        std::unique_lock<std::mutex> l(lock);
        while(open && (!queue_size.load() || q.empty())) {
            if(timeout) {
                auto time = timeout.get(1)
                    *std::chrono::milliseconds(1);
                if(std::cv_status::timeout ==
                   notify.wait_for(l,time)) {
                    /* std::cout << "Timed out\n"; */
                    return {};
                }
            } else {
                notify.wait(l);
            }
        }
        if(!open) { return {}; }

        Maybe<T> ret(q.front());
        q.pop();
        --queue_size;
        return ret;
    }

    void close() {
        std::lock_guard<std::mutex> l(lock);
        open = false;
        notify.notify_all();
    }
};

#endif

