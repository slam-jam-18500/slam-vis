#include "Entity.hpp"

void Entity::applyKinematics(Entity& e,float dt) {
    Vec3f netForce = Vec3f::ZERO;
    for(auto force:e.forces) {
        netForce += force;
    }
    e._loc += e._vel*dt + 0.5f*netForce*dt*dt;
    e._vel += netForce*dt;
}

void Entity::handleCollision(Collision c,Entity& e) {
    
}

void Entity::update(float dt) {
    Entity::applyKinematics(*this,dt);
    _update(dt);
}

